import * as admin from 'firebase-admin';

export class Utils {

    static removeUndefinedFields(object: any) {
        Object.keys(object).forEach(key => {
            if (object[key] === undefined) {
                delete object[key];
            }
        });
    }
    static addFirebaseDeleteToUndefinedFields(object: any) {
        Object.keys(object).forEach(key => {
            if (object[key] === undefined) {
                object[key] = admin.firestore.FieldValue.delete();
            }
        });
    }
}
