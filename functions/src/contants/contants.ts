
export const Constants = {
    RESPONSE: {
        CANT_UPDATED: 'You can\'t update this object',
        CANT_DELETED: 'You can\'t delete this object',
        OBJECT_DELETED: 'This object was deleted place',
        VALUE_INVALID: 'Some info is invalid',
        NOT_FOUND: 'Object not found',
        SUCCESS: 'The operation is done',
        FAILURE: 'The operation has failed',
        SESSION_EXPIRED: 'Your session is expired',
        SESSION_INVALID: 'Session invalid',
        USER_NOT_REGISTERED: 'User is not registered',
        USER_ALREADY_REGISTERED: 'This user is already registered',
        HAS_NO_PRIVILEGES: 'This user does not have permissions for this action',
        PLACE_IS_NOT_WAITING: 'This place is not in a waiting state',
        PLACE_STATUS_ERROR: 'Unrecognized status',
        PLAN_NOT_AVAILABLE: 'This plan is unavailable',
        PLACE_NOT_AVAILABLE: 'This place is unavailable',
        CLAIM_NOT_REGISTERED: 'Here there are no claims for this user',
        CANT_REGISTER_PLACE_TODAY: 'You have already registered 10 places today',
        PAYMENT_REGISTERED_SUCCESSFULLY: 'You payment was registered successfully',
        PAYMENT_REJECTED_SUCCESSFULLY: 'You payment was rejected successfully',
        PAYMENT_ACCEPTED_SUCCESSFULLY: 'You payment was accepted successfully',
        CODE: {
            NOT_REGISTERED: 40,
            ALREADY_REGISTERED: 41,
            SESSION_EXPIRED: 50,
            SESSION_INVALID: 51,
            VALUE_INVALID: 61,
            CLAIM_NOT_REGISTERED: 71,
            CANT_REGISTER_PLACE_TODAY: 31
        }
    },
    HTTP_STATUS: {
        OK: 200,
        UNAUTHORIZED: 401,
        NOT_FOUND: 404,
        UNPROCESSABLE_ENTITY: 422,
        ERROR: 500,
    },
    ADMIN_AUTH: {
        RESPONSE_CODES: {
            TOKEN_EXPIRED: 'auth/id-token-expired'
        }
    },
    PLACE_STATUS: {
        INCOMPLETE: 'INCOMPLETE',
        WAITING: 'WAITING',
        ACCEPTED: 'ACCEPTED',
        VERIFIED: 'VERIFIED',
        REJECTED: 'REJECTED',
        DISABLED: 'DISABLED',
        EXPIRED: 'EXPIRED',
    },
    USER_STATUS: {
        REGISTERED: 'REGISTERED',
        ACTIVE: 'ACTIVE',
        DISABLED: 'DISABLED',
    },
    USER_TYPE: {
        NORMAL: 'NORMAL',
        ADMIN: 'ADMIN'
    },
    CLAIM_STATUS: {
        WAITING: 'WAITING',
        ACCEPTED: 'ACCEPTED',
        REJECTED: 'REJECTED',
    },
    NOTIFICATIONS: {
        MESSAGES: {
            PLACE_ACCEPTED: 'Tu registro de empresa ha sido aceptado.',
            PLACE_REJECTED: 'Tu registro de empresa ha sido rechazado.',
            PAYMENT_ACCEPTED: 'El pago que realizaste para activar tu empresas ha sido validado correctamente.',
            PAYMENT_REJECTED: 'El pago que informaste ha sido marcado como no realizado.',
        },
        TYPE: {
            PLACE_ACCEPTED: 'PLACE_ACCEPTED',
            PLACE_REJECTED: 'PLACE_REJECTED',
            PAYMENT_ACCEPTED: 'PAYMENT_ACCEPTED',
            PAYMENT_REJECTED: 'PAYMENT_REJECTED'
        }

    },
    PLACE: {
        MAXIMUM_DAILY_RECORDS: 30
    },
    PAYMENTS: {
        TYPE: {
            TRANSFER: 'TRANSFER',
            CASH: 'CASH',
            GOOGLE_PAY: 'GOOGLE_PAY'
        },
        STATUS: {
            PENDING: 'PENDING',
            REJECTED: 'REJECTED',
            ACCEPTED: 'ACCEPTED',
        }
    }
};
