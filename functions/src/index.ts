import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as cors from 'cors';
import * as express from 'express';
import { authRoute } from './api/core/auth/auth.route';
import { placesRoute } from './api/core/places/places.route';
import { usersRoute } from './api/core/users/users.route';
import { plansRoute } from './api/core/plans/plans.route';
import { claimsRoute } from './api/core/claims/claims.route';
import { notificationsRoute } from './api/core/notifications/notifications.route';
import { productsRoute } from './api/core/products/products.route';
import { authenticate } from "./api/middlewares/authenticate";
import { errorHandler } from "./api/middlewares/error";
import { currenciesRoute } from './api/core/currencies/currencies.route';
import { configRoute } from './api/core/config/config.route';
import { paymentsRoute } from './api/core/payments/payments.route';
import { servicesRoute } from './api/core/services/services.route';

// Initializes Cloud Functions.
admin.initializeApp(functions.config().firebase);

// Firestore settings.
export const db = admin.firestore();
db.settings({timestampsInSnapshots: true});

// Express settings.
const app = express();
const corsHandler = cors({origin: true});

// add cors
app.use(corsHandler);

// add verify session
app.use(authenticate);

// add routers
app.use(authRoute);
app.use(placesRoute);
app.use(usersRoute);
app.use(plansRoute);
app.use(claimsRoute);
app.use(notificationsRoute);
app.use(productsRoute);
app.use(currenciesRoute);
app.use(configRoute);
app.use(paymentsRoute);
app.use(servicesRoute);

// add error handler
app.use(errorHandler);

export const api = functions.https.onRequest(app);
