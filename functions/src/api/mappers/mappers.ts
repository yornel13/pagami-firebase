import { ServerResponse } from "../core/base/server.response";

export class Mappers {

    static responsePassed(response: any): ServerResponse {
        return {
            passed: true,
            response
        }
    }

    static response(message: string, passed: boolean = false, code?: number): ServerResponse {
        return {
            passed,
            message,
            code
        }
    }
}
