import { NextFunction, Request, Response } from "express";
import { Mappers } from "../mappers/mappers";

export const errorHandler = (err: any, req: Request, res: Response, next: NextFunction) => {
    res.status(err.status || 500).json(
        Mappers.response(err.message)
    );
};
