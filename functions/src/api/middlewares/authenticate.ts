import { Request, Response, NextFunction } from "express";
import { SessionService } from "../core/base/session.service";

const sessionService = SessionService;

export const authenticate = async (req: Request, res: Response, next: NextFunction) => {
    if (await sessionService.verify(req, res)) {
        next();
    }
};
