import {BaseRepository} from '../base/base.repository';
import { db } from "../../../index";
import * as admin from "firebase-admin";
import {Constants} from "../../../contants/contants";

export class UsersRepository extends BaseRepository {

    static entity = 'users';

    static async findById(id: string) {
        const doc = await db.collection(this.entity).doc(id).get();
        if (doc.exists) {
            const user = this.mapDocToObject(doc);
            delete user.id;
            return user;
        } else {
            return null;
        }
    }

    static async createUser(id: string, object: any) {
        await db.collection(this.entity).doc(id).set(object);
        return object;
    }

    static async changeType(id: string, type: string) {
        await db.collection(this.entity).doc(id).update({
            lastUpdate: admin.firestore.FieldValue.serverTimestamp(),
            type
        });
        return true;
    }

    static async changeStatus(id: string, status: string) {
        await db.collection(this.entity).doc(id).update({
            lastUpdate: admin.firestore.FieldValue.serverTimestamp(),
            status
        });
        return true;
    }

    static async countActive(): Promise<number> {
        const snapshot = await db.collection(this.entity)
            .where('status', '==', Constants.USER_STATUS.ACTIVE)
            .select().get();
        return snapshot.docs.length;
    }

    static async countDisabled(): Promise<number> {
        const snapshot = await db.collection(this.entity)
            .where('status', '==', Constants.USER_STATUS.DISABLED)
            .select().get();
        return snapshot.docs.length;
    }

    static async update(id: string, object: any) {
        delete object.totalPlaces;
        return super.update(id, object)
    }

    static async changeUserRegisteredPlaces(id: string, total: number) {
        return super.update(id, { totalPlaces: total })
    }
}
