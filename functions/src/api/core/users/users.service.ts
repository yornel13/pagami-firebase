import { UsersRepository } from './users.repository';
import { Constants } from '../../../contants/contants';
import { UserCreateDto } from "./domain/dto/user.create.dto";
import * as admin from "firebase-admin";
import { ServerResponse } from "../base/server.response";
import { Mappers } from "../../mappers/mappers";
import { UserUpdateDto } from "./domain/dto/user.update.dto";
import { CountUsers } from "./domain/response/count.users";
import { ClaimsService } from "../claims/claims.service";
import { PlacesService } from "../places/places.service";

const repository = UsersRepository;

export class UsersService {

    /**
     * create user
     * this method create a new user
     */
    static async create(id: string, userDto: UserCreateDto): Promise<ServerResponse> {
        userDto.createTime = admin.firestore.FieldValue.serverTimestamp();
        userDto.lastUpdate = admin.firestore.FieldValue.serverTimestamp();
        userDto.status = Constants.USER_STATUS.ACTIVE;
        userDto.totalPlaces = 0;
        await repository.createUser(id, userDto);
        const userResponse = await repository.findById(id);
        return Mappers.responsePassed(userResponse);
    }
    /**
     * save user
     * this method create a new user
     */
    static async save(userDto: UserCreateDto): Promise<ServerResponse> {
        userDto.createTime = admin.firestore.FieldValue.serverTimestamp();
        userDto.lastUpdate = admin.firestore.FieldValue.serverTimestamp();
        userDto.status = Constants.USER_STATUS.REGISTERED;
        const id: string = await repository.save(userDto);
        const userResponse = await repository.findById(id);
        return Mappers.responsePassed(userResponse);
    }

    /**
     * update user
     * this method update a user
     */
    static async update(userId: string, userDto: UserUpdateDto): Promise<ServerResponse> {
        userDto.lastUpdate = admin.firestore.FieldValue.serverTimestamp();
        await repository.update(userId, userDto);
        const userResponse = await repository.findById(userId);
        return Mappers.responsePassed(userResponse);
    }

    /**
     * update user status
     * this method update status of a user
     */
    static async changeStatus(userId: string, status: string): Promise<ServerResponse> {
        if (status === Constants.USER_STATUS.ACTIVE || status === Constants.USER_STATUS.DISABLED) {
            await repository.changeStatus(userId, status);
            const userResponse = await repository.findById(userId);
            return Mappers.responsePassed(userResponse);
        } else {
            return Mappers.response(Constants.RESPONSE.CANT_UPDATED);
        }
    }

    /**
     * update user type
     * this method update type of a user
     */
    static async changeType(userId: string, type: string): Promise<ServerResponse> {
        if (type === Constants.USER_TYPE.ADMIN || type === Constants.USER_TYPE.NORMAL) {
            await repository.changeType(userId, type);
            const userResponse = await repository.findById(userId);
            return Mappers.responsePassed(userResponse);
        } else {
            return Mappers.response(Constants.RESPONSE.CANT_UPDATED);
        }
    }

    /**
     * find user by id
     * this method return a user by id
     */
    static async findById(id: string): Promise<ServerResponse> {
        const object = await repository.findById(id);
        if (object !== undefined && object !== null) {
            return Mappers.responsePassed(object);
        } else {
            return Mappers.response(Constants.RESPONSE.NOT_FOUND);
        }
    }

    /**
     * get all places
     * this method return all places
     */
    static async getAll(): Promise<any> {
        const places = await repository.getAll();
        return Mappers.responsePassed(places);
    }

    /**
     * delete user by id
     * this method delete a user by id
     */
    static async delete(id: string): Promise<any> {
        await repository.delete(id);
        await ClaimsService.removeUserClaimPlace(id);
        await PlacesService.deleteAllIncompleteOrWaitingByUser(id);
        return Mappers.responsePassed(Constants.RESPONSE.SUCCESS);
    }

    /**
     * exist user
     * this method return true is user exits
     */
    static async exists(uid: string): Promise<boolean> {
        return await repository.exist(uid);
    }

    /**
     * user is admin
     * this method return true is user is admin
     */
    static async isAdmin(uid: string): Promise<boolean> {
        const user = await repository.findById(uid);
        return user && user.type === Constants.USER_TYPE.ADMIN;
    }

    /**
     * count users states
     */
    static async countUsers() {
        const countData: CountUsers = {
            active: await repository.countActive(),
            disabled: await repository.countDisabled()
        };
        return Mappers.responsePassed(countData);
    }

    static async increaseUserRegisteredPlaces(uid: string) {
        const user = await repository.findById(uid);
        const total = Number.parseInt(user.totalPlaces) + 1;
        await repository.changeUserRegisteredPlaces(uid, total);
    }

    /**
     * update today records object from user
     * this method update a today record user
     */
    static async updateTodayRecords(userId: string, recordsToday: any): Promise<any> {
        await repository.update(userId, { recordsToday });
    }
}
