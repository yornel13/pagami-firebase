import { Request, Response, NextFunction } from "express";
import { Constants } from "../../../contants/contants";
import { ServerResponse } from "../base/server.response";
import { UsersService } from "./users.service";
import {Mappers} from "../../mappers/mappers";

const service = UsersService;

export class UsersController {

    static async save(req: Request, res: Response, next: NextFunction) {
        try {
            const userDto = req.body;
            const response = await service.save(userDto);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    };

    static async update(req: Request, res: Response, next: NextFunction) {
        try {
            const userDto = req.body;
            const response: ServerResponse = await service.update(userDto.id, userDto);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    };

    static async changeStatus(req: Request, res: Response, next: NextFunction) {
        try {
            const uid: any = req.headers.uid;
            const id = req.params.id;
            const status = req.params.status;
            if (await service.isAdmin(uid)) {
                const response: ServerResponse = await service.changeStatus(id, status);
                res.status(Constants.HTTP_STATUS.OK).json(response);
            } else {
                res.status(Constants.HTTP_STATUS.OK).json(Mappers.response(Constants.RESPONSE.HAS_NO_PRIVILEGES));
            }
        } catch (e) {
            next(e);
        }
    };

    static async changeType(req: Request, res: Response, next: NextFunction) {
        try {
            const uid: any = req.headers.uid;
            const id = req.params.id;
            const type = req.params.type;
            if (await service.isAdmin(uid)) {
                const response: ServerResponse = await service.changeType(id, type);
                res.status(Constants.HTTP_STATUS.OK).json(response);
            } else {
                res.status(Constants.HTTP_STATUS.OK).json(Mappers.response(Constants.RESPONSE.HAS_NO_PRIVILEGES));
            }
        } catch (e) {
            next(e);
        }
    };

    static async findById(req: Request, res: Response, next: NextFunction) {
        try {
            const id = req.params.id;
            const response = await service.findById(id);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    };

    static async getAll(req: Request, res: Response, next: NextFunction) {
        try {
            const response = await service.getAll();
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    };

    static async delete(req: Request, res: Response, next: NextFunction) {
        try {
            const id = req.params.id;
            const response = await service.delete(id);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    };

    static async count(req: Request, res: Response, next: NextFunction) {
        try {
            const response = await service.countUsers();
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    };
}
