export interface UserCreateDto {
    name: string;
    lastname: string;
    photoUrl: string;
    email: string;
    phone: string;
    location: string;
    notifications?: boolean;
    fillOrders?: boolean;
    terms?: boolean;
    status?: string;
    createTime?: any;
    lastUpdate?: any;
    totalPlaces: any;
}
