export interface UserUpdateDto {
    name: string;
    lastname: string;
    photoUrl: string;
    email: string;
    phone: string;
    location: string;
    notifications?: boolean;
    fillOrders?: boolean;
    terms?: boolean;
    lastUpdate?: any;
}
