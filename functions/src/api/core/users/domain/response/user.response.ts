export interface UserResponse {
    name: string;
    lastname: string;
    email: string;
    location: string;
    phone: string;
    photoUrl: string;
    state: string;
    createTime: Date;
    lastUpdate: Date;
    notifications: boolean;
    fillOrders: boolean;
    terms: boolean;
    type: string;
    totalPlaces: number;
    recordsToday?: {
        day: any;
        count: number;
    }
}
