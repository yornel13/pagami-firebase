import * as express from 'express';
import * as asyncHandler from 'express-async-handler'
import { UsersController } from "./users.controller";

const basePath = '/users';
const app = express();
const controller = UsersController;

app.get(`${basePath}`, asyncHandler(async (req, res, next) => {
    await controller.getAll(req, res, next);
}));

app.get(`${basePath}/count`, asyncHandler(async (req, res, next)  => {
    await controller.count(req, res, next);
}));

app.get(`${basePath}/:id`, asyncHandler(async (req, res, next)  => {
    await controller.findById(req, res, next);
}));

app.post(`${basePath}`, asyncHandler(async (req, res, next)  => {
    await controller.save(req, res, next);
}));

app.put(`${basePath}`, asyncHandler(async (req, res, next)  => {
    await controller.update(req, res, next);
}));

app.put(`${basePath}/:id/status/:status`, asyncHandler(async (req, res, next)  => {
    await controller.changeStatus(req, res, next);
}));

app.put(`${basePath}/:id/type/:type`, asyncHandler(async (req, res, next)  => {
    await controller.changeType(req, res, next);
}));

app.delete(`${basePath}/:id`, asyncHandler(async (req, res, next)  => {
    await controller.delete(req, res, next);
}));

export const usersRoute = app;
