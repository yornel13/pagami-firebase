import { Request, Response, NextFunction } from "express";
import { Constants } from "../../../contants/contants";
import { ServerResponse } from "../base/server.response";
import { AuthService } from "./auth.service";
import { UserValidation } from "../../validations/user.validation";

const service = AuthService;

export class AuthController {

    static async signIn(req: Request, res: Response, next: NextFunction) {
        try {
            const uid: any = req.headers.uid;
            const response: ServerResponse = await service.signIn(uid);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    };

    static async createUser(req: Request, res: Response, next: NextFunction) {
        try {
            const uid: any = req.headers.uid;
            const userDto = req.body;
            if (UserValidation.isValid(req, res)) {
                const response: ServerResponse = await service.createUser(uid, userDto);
                res.status(Constants.HTTP_STATUS.OK).json(response);
            }
        } catch (e) {
            next(e);
        }
    };

    static async updateUser(req: Request, res: Response, next: NextFunction) {
        try {
            const uid: any = req.headers.uid;
            const userDto = req.body;
            if (UserValidation.isValid(req, res)) {
                const response: ServerResponse = await service.updateUser(uid, userDto);
                res.status(Constants.HTTP_STATUS.OK).json(response);
            }
        } catch (e) {
            next(e);
        }
    };

    static async verifyUser(req: Request, res: Response, next: NextFunction) {
        try {
            const uid: any = req.headers.uid;
            const response: ServerResponse = await service.verifyUser(uid);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    };

    static async delete(req: Request, res: Response, next: NextFunction) {
        try {
            const uid: any = req.headers.uid;
            const response: ServerResponse = await service.delete(uid);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    };
}
