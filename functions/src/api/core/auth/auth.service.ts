import { Constants } from '../../../contants/contants';
import { UsersService } from "../users/users.service";
import { ServerResponse } from "../base/server.response";
import { Mappers } from "../../mappers/mappers";
import { UserCreateDto } from "../users/domain/dto/user.create.dto";
import { UserUpdateDto } from "../users/domain/dto/user.update.dto";

const usersService = UsersService;

export class AuthService {

    static async signIn(uid: string): Promise<ServerResponse> {
        const response: ServerResponse = await usersService.findById(uid);
        if (response.passed) {
            return response;
        } else {
            return Mappers.response(
                Constants.RESPONSE.USER_NOT_REGISTERED,
                false,
                Constants.RESPONSE.CODE.NOT_REGISTERED
            );
        }
    }

    static async createUser(uid: string, userDto: UserCreateDto): Promise<ServerResponse> {
        if (!await usersService.exists(uid)) {
            return await usersService.create(uid, userDto);
        } else {
            return Mappers.response(
                Constants.RESPONSE.USER_ALREADY_REGISTERED,
                false,
                Constants.RESPONSE.CODE.ALREADY_REGISTERED
            );
        }
    }

    static async updateUser(uid: string, userDto: UserUpdateDto): Promise<ServerResponse> {
        if (await usersService.exists(uid)) {
            return await usersService.update(uid, userDto);
        } else {
            return Mappers.response(
                Constants.RESPONSE.USER_NOT_REGISTERED,
                false,
                Constants.RESPONSE.CODE.NOT_REGISTERED
            );
        }
    }

    static async verifyUser(uid: string): Promise<ServerResponse> {
        return await usersService.findById(uid);
    }

    static async delete(uid: string): Promise<ServerResponse> {
        if (await usersService.exists(uid)) {
            return await usersService.delete(uid);
        } else {
            return Mappers.response(
                Constants.RESPONSE.USER_NOT_REGISTERED,
                false,
                Constants.RESPONSE.CODE.NOT_REGISTERED
            );
        }
    }
}
