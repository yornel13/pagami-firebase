import * as express from 'express';
import { AuthController } from "./auth.controller";

const basePath = '/auth';
const app = express();
const controller = AuthController;

app.post(`${basePath}/sign-in`, async (req, res, next)  => {
    await controller.signIn(req, res, next);
});

app.post(`${basePath}/create/user`, async (req, res, next)  => {
    await controller.createUser(req, res, next);
});

app.post(`${basePath}/update/user`, async (req, res, next)  => {
    await controller.updateUser(req, res, next);
});

app.get(`${basePath}/verify/user`, async (req, res, next)  => {
    await controller.verifyUser(req, res, next);
});

app.delete(`${basePath}/delete/user`, async (req, res, next)  => {
    await controller.delete(req, res, next);
});

export const authRoute = app;
