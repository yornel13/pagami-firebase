import { Request, Response, NextFunction } from "express";
import { Constants } from "../../../contants/contants";
import { PaymentsService } from './payments.service';
import { ServerResponse } from '../base/server.response';
import { Mappers } from '../../mappers/mappers';

const service = PaymentsService;

export class PaymentsController {

    static async getAllByStatus(req: Request, res: Response, next: NextFunction) {
        try {
            const status: string = req.params.status;
            const response = await service.getAllByStatus(status);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async getAllFinish(req: Request, res: Response, next: NextFunction) {
        try {
            const response = await service.getAllFinish();
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async findById(req: Request, res: Response, next: NextFunction) {
        try {
            const id = req.params.id;
            const response = await service.findById(id);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    };

    static async save(req: Request, res: Response, next: NextFunction) {
        try {
            const paymentDto = req.body;
            const uid: any = req.headers.uid;
            const response = await service.save(uid, paymentDto);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async processPayment(req: Request, res: Response, next: NextFunction) {
        try {
            const claimId: string = req.params.id;
            const status: string  = req.params.status;
            if (status === Constants.PAYMENTS.STATUS.ACCEPTED) {
                const response: ServerResponse = await service.accepted(claimId);
                res.status(Constants.HTTP_STATUS.OK).json(response);
            } else if (status === Constants.PAYMENTS.STATUS.REJECTED) {
                const response: ServerResponse = await service.rejected(claimId);
                res.status(Constants.HTTP_STATUS.OK).json(response);
            } else {
                res.status(Constants.HTTP_STATUS.OK).json(Mappers.response(Constants.RESPONSE.PLACE_STATUS_ERROR));
            }
        } catch (e) {
            next(e);
        }
    }

    static async getPendingByUserAndPlace(req: Request, res: Response, next: NextFunction) {
        try {
            const placeId = req.params.placeId;
            const uid: any = req.headers.uid;
            const response = await service.getPendingByUserAndPlace(uid, placeId);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }
}
