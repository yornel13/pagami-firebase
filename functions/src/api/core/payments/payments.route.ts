import * as express from 'express';
import * as asyncHandler from 'express-async-handler';
import { PaymentsController } from './payments.controller';

const basePath = '/payments';
const app = express();
const controller = PaymentsController;

app.get(`${basePath}/PENDING/place/:placeId`, asyncHandler(async (req, res, next) => {
    await controller.getPendingByUserAndPlace(req, res, next);
}));

app.get(`${basePath}/status/:status`, asyncHandler(async (req, res, next) => {
    await controller.getAllByStatus(req, res, next);
}));

app.get(`${basePath}/all-finish`, asyncHandler(async (req, res, next) => {
    await controller.getAllFinish(req, res, next);
}));

app.get(`${basePath}/:id`, asyncHandler(async (req, res, next) => {
    await controller.findById(req, res, next);
}));

app.post(`${basePath}`, asyncHandler(async (req, res, next)  => {
    await controller.save(req, res, next);
}));

app.put(`${basePath}/:id/status/:status`, asyncHandler(async (req, res, next) => {
    await controller.processPayment(req, res, next);
}));

export const paymentsRoute = app;
