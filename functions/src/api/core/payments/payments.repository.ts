import {BaseRepository} from '../base/base.repository';
import {db} from '../../../index';
import { Constants } from '../../../contants/contants';

export class PaymentsRepository extends BaseRepository {

    static entity = 'payments';

    static async getAllByStatus(status: string) {
        const snapshot = await db.collection(this.entity)
            .where('status', '==', status)
            .get();
        const list: any[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        return list;
    }

    static async getAllFinish() {
        const snapshot = await db.collection(this.entity)
            .where('status', 'in', [Constants.PAYMENTS.STATUS.ACCEPTED, Constants.PAYMENTS.STATUS.REJECTED])
            .select('createTime', 'status', 'amount', 'photoUrlToShow', 'nameToShow', 'type', 'currency')
            .get();
        const list: any[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        return list;
    }

    static async getUserAndPlaceAndStatus(userId: string, placeId: string, status: string) {
        const snapshot = await db.collection(this.entity)
            .where('status', '==', status)
            .where('userId', '==', userId)
            .where('placeId', '==', placeId)
            .limit(1).get();
        const list: any[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        if (list.length == 0) {
            return null;
        } else {
            return list[0];
        }
    }
}
