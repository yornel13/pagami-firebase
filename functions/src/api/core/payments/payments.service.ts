import * as admin from 'firebase-admin';
import { Constants } from '../../../contants/contants';
import { ServerResponse } from "../base/server.response";
import { Mappers } from "../../mappers/mappers";
import { PaymentsDto } from './domain/dto/payments.dto';
import { PaymentsRepository } from './payments.repository';
import { PlansService } from '../plans/plans.service';
import { PlanResponse } from '../plans/domain/response/plan.response';
import { PlacePlanDto } from '../places/domain/dto/place.plan.dto';
import { PlacesService } from '../places/places.service';
import { NotificationsService } from '../notifications/notifications.service';
import { PlaceResponse } from '../places/domain/response/place.response';
import { UsersService } from '../users/users.service';

const repository = PaymentsRepository;

export class PaymentsService {

    /**
     * get all payments by verification status
     */
    static async getAllByStatus(status: string): Promise<ServerResponse> {
        const payments = await repository.getAllByStatus(status);
        return Mappers.responsePassed(payments);
    }

    /**
     * get all payments by verification status
     */
    static async getAllFinish(): Promise<ServerResponse> {
        const payments: PaymentsDto[] = await repository.getAllFinish();
        let totalCop = 0;
        let totalUsd = 0;
        let totalTransactionsAccepted = 0;
        let totalTransactionsRejected = 0;
        for (let payment of payments) {
            if (payment.status === Constants.PAYMENTS.STATUS.ACCEPTED) {
                if (payment.currency === 'COP') {
                    totalCop += payment.amount || 0;
                } else if (payment.currency === 'USD') {
                    totalUsd += payment.amount || 0;
                }
                totalTransactionsAccepted++;
            } else {
                totalTransactionsRejected++;
            }
        }
        const response = {
            payments: payments,
            totalIncomeUsd: totalUsd,
            totalIncomeCop: totalCop,
            totalTransactionsAccepted,
            totalTransactionsRejected
        };
        return Mappers.responsePassed(response);
    }

    /**
     * get by id
     * return too user, place and plan
     */
    static async findById(id: string): Promise<ServerResponse> {
        const payment: any = await repository.findById(id);
        if (payment.userId != null) {
            const userServerResponse: ServerResponse = await UsersService.findById(payment.userId);
            if (userServerResponse.passed && userServerResponse.response) {
                payment.user = userServerResponse.response;
            }
        }
        if (payment.placeId != null) {
            const placeServerResponse: ServerResponse = await PlacesService.findById(payment.placeId);
            if (placeServerResponse.passed && placeServerResponse.response) {
                payment.place = placeServerResponse.response;
            }
        }
        if (payment.planId != null) {
            const planServerResponse: ServerResponse = await PlansService.findById(payment.planId);
            if (planServerResponse.passed && planServerResponse.response) {
                payment.plan = planServerResponse.response;
            }
        }
        return Mappers.responsePassed(payment);
    }

    /**
     * save a payment
     * this method create a payment with status pending
     */
    static async save(userId: string, paymentsDto: PaymentsDto): Promise<ServerResponse> {
        paymentsDto.createTime = admin.firestore.FieldValue.serverTimestamp();
        paymentsDto.lastUpdate = admin.firestore.FieldValue.serverTimestamp();
        paymentsDto.validated = false;
        paymentsDto.userId = userId;
        paymentsDto.status = Constants.PAYMENTS.STATUS.PENDING;
        const placeServerResponse: ServerResponse = await PlacesService.findById(paymentsDto.placeId);
        if (placeServerResponse.passed && placeServerResponse.response) {
            const place: PlaceResponse = placeServerResponse.response;
            paymentsDto.photoUrlToShow = place.photoUrl;
            paymentsDto.nameToShow = place.name;
        }
        const planServerResponse: ServerResponse = await PlansService.findById(paymentsDto.planId);
        if (planServerResponse.passed && planServerResponse.response) {
            const plan: PlanResponse = planServerResponse.response;
            if (paymentsDto.currency === 'COP') {
                paymentsDto.planAmount = plan.amountCop;
            } else {
                paymentsDto.planAmount = plan.amount;
            }
        }
        await repository.save(paymentsDto);
        return Mappers.responsePassed(Constants.RESPONSE.PAYMENT_REGISTERED_SUCCESSFULLY);
    }

    /**
     * rejected a payment
     */
    static async rejected(paymentId: string): Promise<ServerResponse> {
        const payment: PaymentsDto = await repository.findById(paymentId);
        payment.validated = true;
        payment.status = Constants.PAYMENTS.STATUS.REJECTED;
        payment.lastUpdate = admin.firestore.FieldValue.serverTimestamp();
        await repository.update(paymentId, payment);
        await NotificationsService.createNotificationPaymentRejected(payment.userId!, payment.placeId, paymentId);
        return Mappers.responsePassed(Constants.RESPONSE.PAYMENT_REJECTED_SUCCESSFULLY);
    }

    /**
     * validated a payment
     */
    static async accepted(paymentId: string): Promise<ServerResponse> {
        const payment = await repository.findById(paymentId);
        payment.validated = true;
        payment.status = Constants.PAYMENTS.STATUS.ACCEPTED;
        payment.lastUpdate = admin.firestore.FieldValue.serverTimestamp();
        await this.setupPlan(payment);
        await repository.update(paymentId, payment);
        await NotificationsService.createNotificationPaymentAccepted(payment.userId, payment.placeId, paymentId);
        return Mappers.responsePassed(Constants.RESPONSE.PAYMENT_ACCEPTED_SUCCESSFULLY);
    }

    private static async setupPlan(payment: PaymentsDto) {
        const plan: PlanResponse = (await PlansService.findById(payment.planId)).response;
        const placePlan: PlacePlanDto = {};
        placePlan.limitProducts = plan.maxProducts;
        placePlan.limitServices = plan.maxServices;
        placePlan.remainDays = plan.days;
        placePlan.activateDate = admin.firestore.FieldValue.serverTimestamp();
        placePlan.lastCheckDate = admin.firestore.FieldValue.serverTimestamp();
        placePlan.isActive = true;
        placePlan.planId = plan.id;
        await PlacesService.updatePlan(payment.placeId, placePlan);
    }

    /**
     * get payment pending by place and user
     * this method return a plan
     */
    static async getPendingByUserAndPlace(userId: string, placeId: string): Promise<ServerResponse> {
        const payment = await repository.getUserAndPlaceAndStatus(userId, placeId, Constants.PAYMENTS.STATUS.PENDING);
        return Mappers.responsePassed(payment);
    }
}
