export interface PaymentsResponse {
    id: string;
    name: string;
    amount: number;
    days: number;
    daysText: string;
    daysSubText: string;
    promotionText?: string;
    active: boolean;
}
