export interface PaymentsDto {
    id?: string;
    type: string;
    planId: string;
    planAmount?: number;
    userId?: string;
    placeId: string;
    date: boolean;
    number?: boolean;
    note?: string;
    photo?: string;
    amount: number;
    createTime?: any;
    lastUpdate?: any;
    validated?: boolean;
    status?: string;
    photoUrlToShow?: string;
    nameToShow?: string;
    currency?: string;
}
