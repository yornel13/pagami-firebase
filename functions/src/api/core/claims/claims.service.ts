import { ServerResponse } from '../base/server.response';
import { Mappers } from '../../mappers/mappers';
import { ClaimDto } from './domain/dto/claim.dto';
import { Constants } from '../../../contants/contants';
import { ClaimStatus, PlaceResponse } from '../places/domain/response/place.response';
import * as admin from 'firebase-admin';
import { ClaimsRepository } from './claims.repository';
import { UserResponse } from '../users/domain/response/user.response';
import { UsersService } from "../users/users.service";
import { PlacesService } from "../places/places.service";
import { PlansService } from '../plans/plans.service';

const repository = ClaimsRepository;

export class ClaimsService {

    /**
     * get all claims active
     * this method return all plans active
     */
    static async claim(claimDto: ClaimDto): Promise<ServerResponse> {
        const place: PlaceResponse = (await PlacesService.findById(claimDto.placeId)).response;
        if (place && place.status === Constants.PLACE_STATUS.ACCEPTED && place.claim === undefined) {
            const user: UserResponse = (await UsersService.findById(claimDto.userId)).response;
            claimDto.createTime = admin.firestore.FieldValue.serverTimestamp();
            claimDto.lastUpdate = admin.firestore.FieldValue.serverTimestamp();
            claimDto.status = Constants.PLACE_STATUS.WAITING;
            claimDto.placeName = place.name;
            claimDto.placePhoto = place.photoUrl;
            claimDto.userName = `${user.name} ${user.lastname}`;
            claimDto.userPhoto = user.photoUrl;
            const claimId: string = await repository.save(claimDto);
            const claimStatus: ClaimStatus = {
                status: Constants.PLACE_STATUS.WAITING,
                userId: claimDto.userId,
                createTime: admin.firestore.FieldValue.serverTimestamp(),
                claimId: claimId,
            };
            await PlacesService.claim(claimDto.placeId, claimStatus);
            const claim = await repository.findById(claimId);
            return Mappers.responsePassed(claim);
        } else {
            return Mappers.response(Constants.RESPONSE.PLACE_NOT_AVAILABLE);
        }
    }

    /**
     * accept claim
     */
    static async acceptClaim(uid: string, claimId: string): Promise<ServerResponse> {
        if (await UsersService.isAdmin(uid)) {
            const claim: ClaimDto = await repository.findById(claimId);
            if (claim.status === Constants.CLAIM_STATUS.WAITING) {
                claim.status = Constants.CLAIM_STATUS.ACCEPTED;
                claim.lastUpdate = admin.firestore.FieldValue.serverTimestamp();
                await PlacesService.makeVerified(claim.placeId);
                await repository.update(claimId, claim);
                return Mappers.responsePassed(Constants.RESPONSE.SUCCESS);
            } else {
                return Mappers.response(Constants.RESPONSE.CLAIM_NOT_REGISTERED);
            }
        } else {
            return Mappers.response(Constants.RESPONSE.HAS_NO_PRIVILEGES);
        }
    }

    /**
     * reject claim
     */
    static async rejectClaim(uid: string, claimId: string): Promise<ServerResponse> {
        if (await UsersService.isAdmin(uid)) {
            const claim: ClaimDto = await repository.findById(claimId);
            if (claim.status === Constants.CLAIM_STATUS.WAITING) {
                claim.status = Constants.CLAIM_STATUS.REJECTED;
                claim.lastUpdate = admin.firestore.FieldValue.serverTimestamp();
                await PlacesService.clearClaim(claim.placeId);
                await repository.update(claimId, claim);
                return Mappers.responsePassed(Constants.RESPONSE.SUCCESS);
            } else {
                return Mappers.response(Constants.RESPONSE.CLAIM_NOT_REGISTERED);
            }
        } else {
            return Mappers.response(Constants.RESPONSE.HAS_NO_PRIVILEGES);
        }
    }

    /**
     * remove claim place
     */
    static async removeUserClaimPlace(userId: string): Promise<any> {
        const listClaim: ClaimDto[] = await repository.getByUserIdIfWaitingOrAccepted(userId);
        for (const claim of listClaim) {
            claim.status = Constants.CLAIM_STATUS.REJECTED;
            claim.lastUpdate = admin.firestore.FieldValue.serverTimestamp();
            await PlacesService.clearClaim(claim.placeId);
            await repository.update(claim.id!, claim);
            Mappers.responsePassed(Constants.RESPONSE.SUCCESS);
        }
    }

    /**
     * get user active claim
     */
    static async getByUserIdIfWaitingOrAccepted(userId: string): Promise<ServerResponse> {
        const listClaim: ClaimDto[] = await repository.getByUserIdIfWaitingOrAccepted(userId);
        if (listClaim && listClaim.length > 0) {
            const claim: any = listClaim[0];
            if (claim.status === Constants.CLAIM_STATUS.ACCEPTED) {
                const place: PlaceResponse = (await PlacesService.findById(claim.placeId)).response;
                claim.place = place;
                await PlansService.calculatePlacePlanRemain(place)
            }
            return Mappers.responsePassed(claim);
        } else {
            return Mappers.response(
                Constants.RESPONSE.CLAIM_NOT_REGISTERED,
                false,
                Constants.RESPONSE.CODE.CLAIM_NOT_REGISTERED
            );
        }
    }

    /**
     * get user active claim
     */
    static async getByStatus(uid: string, status: string): Promise<ServerResponse> {
        if (await UsersService.isAdmin(uid)) {
            const list = await repository.getByStatus(status);
            return Mappers.responsePassed(list);
        } else {
            return Mappers.response(Constants.RESPONSE.HAS_NO_PRIVILEGES);
        }
    }

    /**
     * Delete claims by placeId
     */
    static async deleteByPlaceId(placeId: string): Promise<any> {
        await repository.deleteByPlaceId(placeId);
    }

    /**
     * find claim by id
     * this method return a claim by id
     */
    static async findById(id: string): Promise<ServerResponse> {
        const object = await repository.findById(id);
        if (object !== undefined && object !== null) {
            return Mappers.responsePassed(object);
        } else {
            return Mappers.response(Constants.RESPONSE.NOT_FOUND);
        }
    }
}
