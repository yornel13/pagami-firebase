import {BaseRepository} from '../base/base.repository';
import { db } from '../../../index';
import { Constants } from '../../../contants/contants';
import { ClaimDto } from './domain/dto/claim.dto';

export class ClaimsRepository extends BaseRepository {

    static entity = 'claims';

    static async getByUserIdIfWaitingOrAccepted(userId: string) {
        const snapshot = await db.collection(this.entity)
            .where('userId', '==', userId)
            .where('status', 'in', [Constants.CLAIM_STATUS.WAITING, Constants.CLAIM_STATUS.ACCEPTED])
            .get();
        const list: ClaimDto[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        return list;
    }

    static async getByStatus(status: string) {
        const snapshot = await db.collection(this.entity)
            .where('status', '==', status)
            .get();
        const list: ClaimDto[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        return list;
    }

    static async deleteByPlaceId(placeId: string) {
        const claimCollection = db.collection(this.entity);
        const snapshot = await claimCollection
            .where('placeId', '==', placeId)
            .select().get();
        const batch = db.batch();
        snapshot.forEach((doc: any) => {
            const claimRef = claimCollection.doc(doc.id);
            batch.delete(claimRef);
        });
        await batch.commit();
    }
}
