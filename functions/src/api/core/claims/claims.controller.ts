import { Request, Response, NextFunction } from "express";
import { Constants } from "../../../contants/contants";
import { ClaimsService } from './claims.service';
import { ClaimDto } from './domain/dto/claim.dto';
import { ServerResponse } from '../base/server.response';
import { ClaimValidation } from '../../validations/claim.validation';
import { Mappers } from '../../mappers/mappers';

const service = ClaimsService;

export class ClaimsController {

    static async claim(req: Request, res: Response, next: NextFunction) {
        try {
            if (ClaimValidation.isValid(req, res)) {
                const claimDto: ClaimDto = req.body;
                const userId: any = req.headers.uid;
                claimDto.userId = userId;
                const response: ServerResponse = await service.claim(claimDto);
                res.status(Constants.HTTP_STATUS.OK).json(response);
            }
        } catch (e) {
            next(e);
        }
    }

    static async processClaim(req: Request, res: Response, next: NextFunction) {
        try {
            const claimId: string = req.params.id;
            const status: string  = req.params.status;
            const userId: any = req.headers.uid;
            if (status === Constants.CLAIM_STATUS.ACCEPTED) {
                const response: ServerResponse = await service.acceptClaim(userId, claimId);
                res.status(Constants.HTTP_STATUS.OK).json(response);
            } else if (status === Constants.CLAIM_STATUS.REJECTED) {
                const response: ServerResponse = await service.rejectClaim(userId, claimId);
                res.status(Constants.HTTP_STATUS.OK).json(response);
            } else {
                res.status(Constants.HTTP_STATUS.OK).json(Mappers.response(Constants.RESPONSE.PLACE_STATUS_ERROR));
            }
        } catch (e) {
            next(e);
        }
    }

    static async getUserClaim(req: Request, res: Response, next: NextFunction) {
        try {
            const userId: any = req.headers.uid;
            const response: ServerResponse = await service.getByUserIdIfWaitingOrAccepted(userId);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async getByStatus(req: Request, res: Response, next: NextFunction) {
        try {
            const userId: any = req.headers.uid;
            const status: string  = req.params.status;
            const response: ServerResponse = await service.getByStatus(userId, status);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async findById(req: Request, res: Response, next: NextFunction) {
        try {
            const id = req.params.id;
            const response = await service.findById(id);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }
}
