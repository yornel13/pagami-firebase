export interface ClaimDto {
    id?: string;
    userPhoto?: string;
    userName?: string;
    placePhoto?: string;
    placeName?: string;
    placeId: string;
    businessRuc: string;
    businessPhone: string;
    businessEmail: string;
    businessComment?: string;
    userId: string;
    status?: string;
    createTime?: any;
    lastUpdate?: any;
}
