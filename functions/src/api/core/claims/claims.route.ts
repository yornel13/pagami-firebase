import * as express from 'express';
import * as asyncHandler from 'express-async-handler';
import { ClaimsController } from './claims.controller';

const basePath = '/claims';
const app = express();
const controller = ClaimsController;

app.post(`${basePath}`, asyncHandler(async (req, res, next) => {
    await controller.claim(req, res, next);
}));

app.get(`${basePath}/my-business`, asyncHandler(async (req, res, next) => {
    await controller.getUserClaim(req, res, next);
}));

app.get(`${basePath}/:id`, asyncHandler(async (req, res, next)  => {
    await controller.findById(req, res, next);
}));

app.get(`${basePath}/status/:status`, asyncHandler(async (req, res, next) => {
    await controller.getByStatus(req, res, next);
}));

app.put(`${basePath}/:id/status/:status`, asyncHandler(async (req, res, next)  => {
    await controller.processClaim(req, res, next);
}));


export const claimsRoute = app;
