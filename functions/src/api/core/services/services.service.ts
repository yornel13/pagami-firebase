import * as admin from 'firebase-admin';
import { Constants } from '../../../contants/contants';
import { Mappers } from '../../mappers/mappers';
import { ServicesRepository } from './services.repository';
import { ServiceDto } from './domain/dto/service.dto';
import { PlacesService } from '../places/places.service';
import { ServerResponse } from '../base/server.response';

const repository = ServicesRepository;

export class ServicesService {

    static async save(serviceDto: ServiceDto) {
        if (await PlacesService.validPlaceStatus(serviceDto.placeId, Constants.PLACE_STATUS.VERIFIED)) {
            serviceDto.createTime = admin.firestore.FieldValue.serverTimestamp();
            serviceDto.lastUpdate = admin.firestore.FieldValue.serverTimestamp();
            const id = await repository.save(serviceDto);
            const service = await repository.findById(id);
            return Mappers.responsePassed(service);
        } else {
            return Mappers.response(Constants.RESPONSE.CANT_UPDATED);
        }
    }

    static async changeStatus(serviceId: string, status: boolean) {
        const id = await repository.update(serviceId, {
            status,
            lastUpdate: admin.firestore.FieldValue.serverTimestamp()
        });
        const service = await repository.findById(id);
        return Mappers.responsePassed(service);
    }

    static async update(serviceDto: ServiceDto) {
        if (await repository.exist(serviceDto.id!)) {
            serviceDto.lastUpdate = admin.firestore.FieldValue.serverTimestamp();
            const id = await repository.update(serviceDto.id!, serviceDto);
            const service = await repository.findById(id);
            return Mappers.responsePassed(service);
        } else {
            return Mappers.response(Constants.RESPONSE.CANT_UPDATED);
        }
    }

    /**
     * get services by place id
     */
    static async getByPlaceId(placeId: string): Promise<ServerResponse> {
        const objects = await repository.findByPlaceId(placeId);
        return Mappers.responsePassed(objects);
    }

    /**
     * delete service by id
     */
    static async delete(id: string): Promise<ServerResponse> {
       await repository.delete(id);
       return Mappers.responsePassed(Constants.RESPONSE.SUCCESS);
    }

    /**
     * Delete services by placeId
     */
    static async deleteByPlaceId(placeId: string): Promise<any> {
        await repository.deleteByPlaceId(placeId);
    }
}
