import * as express from 'express';
import * as asyncHandler from 'express-async-handler';
import { ServicesController } from './services.controller';

const basePath = '/services';
const app = express();
const controller = ServicesController;

app.post(`${basePath}`, asyncHandler(async (req, res, next) => {
    await controller.save(req, res, next);
}));

app.put(`${basePath}/:id/status/:status`, asyncHandler(async (req, res, next) => {
    await controller.updateStatus(req, res, next);
}));

app.put(`${basePath}/:id`, asyncHandler(async (req, res, next) => {
    await controller.update(req, res, next);
}));

app.get(`${basePath}/place/:placeId`, asyncHandler(async (req, res, next) => {
    await controller.getByPlaceId(req, res, next);
}));

app.delete(`${basePath}/:id`, asyncHandler(async (req, res, next) => {
    await controller.delete(req, res, next);
}));

export const servicesRoute = app;
