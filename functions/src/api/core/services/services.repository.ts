import { BaseRepository } from '../base/base.repository';
import { db } from '../../../index';

export class ServicesRepository extends BaseRepository {

    static entity = 'services';

    static async findByPlaceId(placeId: string): Promise<any> {
        const snapshot = await db.collection(this.entity).where('placeId', '==', placeId).get();
        const list: any[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        return list;
    }

    static async deleteByPlaceId(placeId: string) {
        const servicesCollection = db.collection(this.entity);
        const snapshot = await servicesCollection
            .where('placeId', '==', placeId)
            .select().get();
        const batch = db.batch();
        snapshot.forEach((doc: any) => {
            const serviceRef = servicesCollection.doc(doc.id);
            batch.delete(serviceRef);
        });
        await batch.commit();
    }
}
