export interface ServiceDto {
    id?: string;
    placeId: string;
    registeredBy?: string;
    name: string;
    description: string;
    price: number;
    localPrice?: number;
    localCurrency?: string;
    active?: boolean;
    available: boolean;
    photoUrl: string;
    createTime?: any;
    lastUpdate?: any;
}
