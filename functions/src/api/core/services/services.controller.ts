import { Request, Response, NextFunction } from "express";
import { Constants } from "../../../contants/contants";
import { ServicesService } from './services.service';
import { ServiceDto } from './domain/dto/service.dto';
import { ServiceValidation } from '../../validations/service.validation';

const service = ServicesService;

export class ServicesController {

    static async save(req: Request, res: Response, next: NextFunction) {
        try {
            if (ServiceValidation.isValid(req, res)) {
                const productDto: ServiceDto = req.body;
                const uid: any = req.headers.uid;
                productDto.registeredBy = uid;
                const response = await service.save(productDto);
                res.status(Constants.HTTP_STATUS.OK).json(response);
            }
        } catch (e) {
            next(e);
        }
    };

    static async updateStatus(req: Request, res: Response, next: NextFunction) {
        try {
            const status: boolean = Boolean(JSON.parse(req.params.status));
            const serviceId: string = req.params.id;
            const response = await service.changeStatus(serviceId, status);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    };

    static async update(req: Request, res: Response, next: NextFunction) {
        try {
            if (ServiceValidation.isValid(req, res, true)) {
                const productDto: ServiceDto = req.body;
                const serviceId: string = req.params.id;
                productDto.id = serviceId;
                const response = await service.update(productDto);
                res.status(Constants.HTTP_STATUS.OK).json(response);
            }
        } catch (e) {
            next(e);
        }
    };

    static async getByPlaceId(req: Request, res: Response, next: NextFunction) {
        try {
            const placeId: string = req.params.placeId;
            const response = await service.getByPlaceId(placeId);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async delete(req: Request, res: Response, next: NextFunction) {
        try {
            const serviceId: string = req.params.id;
            const response = await service.delete(serviceId);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }
}
