import { Request, Response, NextFunction } from "express";
import { PlacesService } from "./places.service";
import { Constants } from "../../../contants/contants";
import { ServerResponse } from "../base/server.response";
import { Mappers } from "../../mappers/mappers";
import { PlaceFilterDto } from "./domain/dto/place.filter.dto";
import { PlaceValidation } from '../../validations/place.validation';

const service = PlacesService;

export class PlacesController {

    static async save(req: Request, res: Response, next: NextFunction) {
        try {
            const placeDto = req.body;
            placeDto.registeredBy = req.headers.uid;
            const response = await service.save(placeDto);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async update(req: Request, res: Response, next: NextFunction) {
        try {
            const placeDto = req.body;
            // @ts-ignore
            const uid: string = req.headers.uid;
            if (PlaceValidation.isValid(req, res)) {
                const response: ServerResponse = await service.update(uid, placeDto.id, placeDto);
                res.status(Constants.HTTP_STATUS.OK).json(response);
            }
        } catch (e) {
            next(e);
        }
    }

    static async updateLocation(req: Request, res: Response, next: NextFunction) {
        try {
            const placeDto = req.body;
            // @ts-ignore
            const uid: string = req.headers.uid;
            if (PlaceValidation.isLocationValid(req, res)) {
                const response: ServerResponse = await service.updateLocation(uid, placeDto.id, placeDto);
                res.status(Constants.HTTP_STATUS.OK).json(response);
            }
        } catch (e) {
            next(e);
        }
    }

    static async updateStatus(req: Request, res: Response, next: NextFunction) {
        try {
            const status: string  = req.params.status;
            const placeId: string = req.params.id;
            const uid: any = req.headers.uid;
            if (status === Constants.PLACE_STATUS.ACCEPTED) {
                const response: ServerResponse = await service.makeAccepted(uid, placeId);
                res.status(Constants.HTTP_STATUS.OK).json(response);
            } else if (status === Constants.PLACE_STATUS.REJECTED) {
                const reason = req.body.reason;
                const response: ServerResponse = await service.makeRejected(uid, placeId, reason);
                res.status(Constants.HTTP_STATUS.OK).json(response);
            } else {
                res.status(Constants.HTTP_STATUS.OK).json(Mappers.response(Constants.RESPONSE.PLACE_STATUS_ERROR));
            }
        } catch (e) {
            next(e);
        }
    };

    static async findById(req: Request, res: Response, next: NextFunction) {
        try {
            const id = req.params.id;
            const response = await service.findById(id);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async getAll(req: Request, res: Response, next: NextFunction) {
        try {
            const response = await service.getAll();
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async getAllAvailable(req: Request, res: Response, next: NextFunction) {
        try {
            const response = await service.getAllAvailable();
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    };

    static async getAllAvailableWithSelect(req: Request, res: Response, next: NextFunction) {
        try {
            const response = await service.getAllAvailableWithSelect();
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    };

    static async getAllByStatus(req: Request, res: Response, next: NextFunction) {
        try {
            const status = req.params.status;
            if (status === 'CLAIM') {
                const response = await service.getAllWithStatusClaimWaiting();
                res.status(Constants.HTTP_STATUS.OK).json(response);
            } else {
                const response = await service.getAllByStatus(status);
                res.status(Constants.HTTP_STATUS.OK).json(response);
            }
        } catch (e) {
            next(e);
        }
    };

    static async getByRegistered(req: Request, res: Response, next: NextFunction) {
        try {
            const uid: any = req.headers.uid;
            const response = await service.getByRegistered(uid);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async getNearby(req: Request, res: Response, next: NextFunction) {
        try {
            const filterDto: PlaceFilterDto = {
                latitude: Number(req.query.latitude),
                longitude: Number(req.query.longitude),
                radius: Number(req.query.radius),
                placeType: req.query.placeType ? String(req.query.placeType) : undefined,
                text: req.query.text ? String(req.query.text) : undefined,
                countryAcronym: req.query.countryAcronym ? String(req.query.countryAcronym) : undefined
            };
            const response = await service.getNearby(filterDto);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    };

    static async delete(req: Request, res: Response, next: NextFunction) {
        try {
            const id = req.params.id;
            const response = await service.delete(id);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async count(req: Request, res: Response, next: NextFunction) {
        try {
            const response = await service.countPlaces();
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async deleteFull(req: Request, res: Response, next: NextFunction) {
        try {
            const userId: any = req.headers.uid;
            const id: string = req.params.id;
            const response = await service.deleteFull(userId, id);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async updateCategory(req: Request, res: Response, next: NextFunction) {
        try {
            const id: string = req.params.id;
            const categoryDto = req.body;
            const response: ServerResponse = await service.updateCategory(id, categoryDto);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async updateFlyer(req: Request, res: Response, next: NextFunction) {
        try {
            const id: string = req.params.id;
            const flyerDto = req.body;
            const response: ServerResponse = await service.updateFlyer(id, flyerDto);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async deleteFlyer(req: Request, res: Response, next: NextFunction) {
        try {
            const id: string = req.params.id;
            const response = await service.deleteFlyer(id);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async updateHours(req: Request, res: Response, next: NextFunction) {
        try {
            const id: string = req.params.id;
            const hoursDto = req.body;
            const response: ServerResponse = await service.updateHours(id, hoursDto);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async deleteHours(req: Request, res: Response, next: NextFunction) {
        try {
            const id: string = req.params.id;
            const response = await service.deleteHours(id);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async updateAllPlacesSearchParams(req: Request, res: Response, next: NextFunction) {
        try {
            const response = await service.updateAllPlacesSearchParams();
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }
}
