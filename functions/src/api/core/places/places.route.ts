import * as express from 'express';
import * as asyncHandler from 'express-async-handler';
import { PlacesController } from "./places.controller";

const basePath = '/places';
const app = express();
const controller = PlacesController;

app.get(`${basePath}`, asyncHandler(async (req, res, next) => {
    await controller.getAll(req, res, next);
}));

app.get(`${basePath}/status/:status`, asyncHandler(async (req, res, next) => {
    await controller.getAllByStatus(req, res, next);
}));

app.get(`${basePath}/by-registered`, asyncHandler(async (req, res, next) => {
    await controller.getByRegistered(req, res, next);
}));

app.get(`${basePath}/all-available`, asyncHandler(async (req, res, next)  => {
    await controller.getAllAvailable(req, res, next);
}));

app.get(`${basePath}/list/all-available`, asyncHandler(async (req, res, next)  => {
    await controller.getAllAvailableWithSelect(req, res, next);
}));

app.get(`${basePath}/count`, asyncHandler(async (req, res, next)  => {
    await controller.count(req, res, next);
}));

app.get(`${basePath}/:id`, asyncHandler(async (req, res, next)  => {
    await controller.findById(req, res, next);
}));

app.post(`${basePath}`, asyncHandler(async (req, res, next)  => {
    await controller.save(req, res, next);
}));

app.put(`${basePath}/update/location`, asyncHandler(async (req, res, next)  => {
    await controller.updateLocation(req, res, next);
}));

app.put(`${basePath}`, asyncHandler(async (req, res, next)  => {
    await controller.update(req, res, next);
}));

app.put(`${basePath}/:id/status/:status`, asyncHandler(async (req, res, next)  => {
    await controller.updateStatus(req, res, next);
}));

app.put(`${basePath}/:id/category`, asyncHandler(async (req, res, next)  => {
    await controller.updateCategory(req, res, next);
}));

app.put(`${basePath}/:id/flyer`, asyncHandler(async (req, res, next)  => {
    await controller.updateFlyer(req, res, next);
}));

app.put(`${basePath}/:id/hours`, asyncHandler(async (req, res, next)  => {
    await controller.updateHours(req, res, next);
}));

app.delete(`${basePath}/:id/full`, asyncHandler(async (req, res, next)  => {
    await controller.deleteFull(req, res, next);
}));

app.delete(`${basePath}/:id/flyer`, asyncHandler(async (req, res, next)  => {
    await controller.deleteFlyer(req, res, next);
}));

app.delete(`${basePath}/:id/hours`, asyncHandler(async (req, res, next)  => {
    await controller.deleteHours(req, res, next);
}));

app.delete(`${basePath}/:id`, asyncHandler(async (req, res, next)  => {
    await controller.delete(req, res, next);
}));

app.get(`${basePath}/nearby/search`, asyncHandler(async (req, res, next)  => {
    await controller.getNearby(req, res, next);
}));

app.get(`${basePath}/update/all-search-params`, asyncHandler(async (req, res, next)  => {
    await controller.updateAllPlacesSearchParams(req, res, next);
}));

export const placesRoute = app;
