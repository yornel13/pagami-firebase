export interface CountPlaces {
    expired: number;
    verified: number;
    accepted: number;
    total: number;
}
