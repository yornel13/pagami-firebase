import { PlaceFlyerDto } from '../dto/place.flyer.dto';
import { PlacePlanDto } from '../dto/place.plan.dto';

export interface PlaceResponse {
    id?: string;
    latitude: number;
    longitude: number;
    accuracy?: number;
    photoUrl?: string;
    name?: string;
    icon?: string;
    location?: {
        address?: string;
        addressLine?: string;
        postalCode?: string;
        acronym?: string;
        country?: string;
        state?: string;
        city?: string;
    };
    phone?: string;
    samePhone?: boolean;
    whatsapp?: string;
    website?: string;
    type?: string;
    status?: string;
    claim?: ClaimStatus;
    createTime?: string;
    lastUpdate?: string;
    rejectedReason?: string;
    registeredBy?: string;
    allowHome?: boolean;
    flyer?: PlaceFlyerDto;
    category?: {
        icon?: string;
        name?: string;
        subCategory?: number;
    },
    plan: PlacePlanDto
}

export interface ClaimStatus {
    status: string;
    userId: string;
    createTime: any;
    claimId: string;
}
