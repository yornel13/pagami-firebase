export interface PlaceFlyerDto {
    textList: string[];
    photoUrl?: string;
    title?: string;
}
