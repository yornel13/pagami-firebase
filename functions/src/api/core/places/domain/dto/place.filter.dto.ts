export interface PlaceFilterDto {
    latitude: number;
    longitude: number;
    radius: number; // kilometers
    placeType?: string;
    text?: string;
    countryAcronym?: string;
}
