export interface PlaceIncompleteDto {
    latitude: number;
    longitude: number;
    accuracy: number;
    registeredBy?: string;
    createTime?: any;
    lastUpdate?: any;
    status?: string;
}
