export interface PlaceUnregisterDto {
    id: string;
    photoUrl?: string;
    name?: string;
    icon?: string;
    location?: {
        address?: string;
        addressLine?: string;
        postalCode?: string;
        acronym?: string;
        country?: string;
        state?: string;
        city?: string;
    };
    phone?: string;
    samePhone?: boolean;
    whatsapp?: string;
    website?: string;
    type?: string;
    status?: string;
    lastUpdate: any;
    allowHome: boolean;
}
