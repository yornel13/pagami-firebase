export interface PlaceCategoryDto {
    category?: {
        icon?: string;
        name?: string;
        subCategory?: string;
    };
    type?: string;
    lastUpdate?: any;
}
