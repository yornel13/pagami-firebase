export interface PlacePlanDto {
    limitProducts?: number;
    limitServices?: number;
    totalProducts?: number;
    totalServices?: number;
    remainDays?: number;
    activateDate?: any;
    lastCheckDate?: any;
    isActive?: boolean;
    planId?: string;
}
