import { BaseRepository } from '../base/base.repository';
import { db } from '../../../index';
import { ClaimStatus, PlaceResponse } from "./domain/response/place.response";
import { Constants } from "../../../contants/contants";
import { GeoCollectionReference, GeoFirestore, GeoQuery, GeoQuerySnapshot } from "geofirestore";
import { PlaceFilterDto } from "./domain/dto/place.filter.dto";
import * as admin from "firebase-admin";
import { PlaceIncompleteDto } from "./domain/dto/place.incomplete.dto";
import { Utils } from '../../../utils/utils';
import { PlaceFlyerDto } from './domain/dto/place.flyer.dto';

export class PlacesRepository extends BaseRepository {

    static entity = 'places';

    static mapDocToObject(doc: any) {
        const object = doc.data();
        object.id = doc.id;
        try {
            if (object.createTime) {
                object.createTime = object.createTime.toDate();
            }
            if (object.lastUpdate) {
                object.lastUpdate = object.lastUpdate.toDate();
            }
            if (object.plan && object.plan.lastCheckDate) {
                object.plan.lastCheckDate = object.plan.lastCheckDate.toDate();
            }
            if (object.plan && object.plan.activateDate) {
                object.plan.activateDate = object.plan.activateDate.toDate();
            }
            delete object.d;
            delete object.g;
            delete object.l;
        } catch (e) {
        }
        return object;
    }

    static mapDocToObjectPartial(doc: any) {
        const object = doc.data();
        object.id = doc.id;
        try {
            if (object.createTime) {
                object.createTime = object.createTime.toDate();
            }
            if (object.lastUpdate) {
                object.lastUpdate = object.lastUpdate.toDate();
            }
            if (object.plan && object.plan.lastCheckDate) {
                object.plan.lastCheckDate = object.plan.lastCheckDate.toDate();
            }
            if (object.plan && object.plan.activateDate) {
                object.plan.activateDate = object.plan.activateDate.toDate();
            }
            delete object.g;
            delete object.l;
        } catch (e) {
        }
        return object;
    }

    static async save(place: PlaceIncompleteDto): Promise<string> {
        const geofirestore: GeoFirestore = new GeoFirestore(db);
        const geocollection: GeoCollectionReference = geofirestore.collection(this.entity);
        const objectRef = await geocollection.add({
            coordinates: new admin.firestore.GeoPoint(place.latitude, place.longitude),
            status: place.status
        });
        await db.collection(this.entity).doc(objectRef.id).update(place);
        return objectRef.id;
    }

    static async update(id: string, object: any): Promise<any> {
        delete object.id;
        delete object.createTime;
        delete object.d;
        delete object.g;
        delete object.l;
        Utils.addFirebaseDeleteToUndefinedFields(object);
        await db.collection(this.entity).doc(id).update(object);
        // if (object.status) {
        //     if (object.type) {
        //         await new GeoFirestore(db).collection(this.entity).doc(id).update({
        //             status: object.status,
        //             type: object.type
        //         });
        //     } else {
        //         await new GeoFirestore(db).collection(this.entity).doc(id).update({status: object.status});
        //     }
        // }
        // if (object.flyer) {
        //     await new GeoFirestore(db).collection(this.entity).doc(id).update({flyer: this.concatFlyer(object.flyer)});
        // }
        return id;
    }

    static async getByRegistered(registeredBy: string) {
        const snapshot = await db.collection(this.entity)
            .where('registeredBy', '==', registeredBy)
            .get();
        const list: any[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        return list;
    }

    static async getByStatus(status: string) {
        const snapshot = await db.collection(this.entity)
            .where('status', '==', status)
            .get();
        const list: PlaceResponse[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        return list;
    }

    static async getWithStatusClaimWaiting() {
        const snapshot = await db.collection(this.entity)
            .where('claim.status', '==', Constants.CLAIM_STATUS.WAITING)
            .get();
        const list: PlaceResponse[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        return list;
    }

    static async getAllAvailable() {
        const snapshot = await db.collection(this.entity)
            .where('status', 'in', [Constants.PLACE_STATUS.VERIFIED, Constants.PLACE_STATUS.ACCEPTED])
            .get();
        const list: PlaceResponse[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        return list;
    }

    static async getAllAvailableWithSelect() {
        const snapshot = await db.collection(this.entity)
            .where('status', 'in', [Constants.PLACE_STATUS.VERIFIED, Constants.PLACE_STATUS.ACCEPTED])
            .select('status', 'photoUrl', 'name', 'createTime', 'claim.status')
            .get();
        const list: PlaceResponse[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        return list;
    }

    static async getNearby(placeFilterDto: PlaceFilterDto) {
        const geofirestore: GeoFirestore = new GeoFirestore(db);
        const geocollection: GeoCollectionReference = geofirestore.collection(this.entity);
        const query: GeoQuery = geocollection.near({
            center: new admin.firestore.GeoPoint(placeFilterDto.latitude, placeFilterDto.longitude),
            radius: placeFilterDto.radius
        }).where('status', 'in', [
            Constants.PLACE_STATUS.VERIFIED,
            Constants.PLACE_STATUS.ACCEPTED,
            Constants.PLACE_STATUS.EXPIRED
        ]);
        if (placeFilterDto.placeType) {
            query.where('type', '==', placeFilterDto.placeType);
        }
        const geoQuerySnapshot: GeoQuerySnapshot = await query.limit(20)
            .get();
        const list: PlaceResponse[] = [];
        geoQuerySnapshot.native.docs.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        return list;
    }

    static async getNearbyFilterText(placeFilterDto: PlaceFilterDto) {
        placeFilterDto.text = placeFilterDto.text!.toLowerCase();
        const snapshot = await db.collection(this.entity)
            .where('status', 'in', [
                Constants.PLACE_STATUS.VERIFIED,
                Constants.PLACE_STATUS.ACCEPTED,
                Constants.PLACE_STATUS.EXPIRED
            ])
            .orderBy('d.searchParams')
            .where('location.acronym', '==', 'CO') // TODO, se debe validar el pais del search
            .get();

        const list: PlaceResponse[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObjectPartial(doc);
            list.push(object);
        });
        return list;
    }

    static async claim(id: string, claim: ClaimStatus): Promise<any> {
        await db.collection(this.entity).doc(id).update({'claim': claim});
        return id;
    }

    static async countVerified(): Promise<number> {
        const snapshot = await db.collection(this.entity)
            .where('status', '==', Constants.PLACE_STATUS.VERIFIED)
            .select().get();
        return snapshot.docs.length;
    }

    static async countAccepted(): Promise<number> {
        const snapshot = await db.collection(this.entity)
            .where('status', '==', Constants.PLACE_STATUS.ACCEPTED)
            .select().get();
        return snapshot.docs.length;
    }

    static async countTotal(): Promise<number> {
        const snapshot = await db.collection(this.entity)
            .where('status', 'in', [Constants.PLACE_STATUS.VERIFIED, Constants.PLACE_STATUS.ACCEPTED, Constants.PLACE_STATUS.EXPIRED])
            .select().get();
        return snapshot.docs.length;
    }

    static async countExpired(): Promise<number> {
        const snapshot = await db.collection(this.entity)
            .where('active', '==', false)
            .select().get();
        return snapshot.docs.length;
    }

    static async deleteAllIncompleteOrWaitingByUser(registeredBy: string) {
        const placeCol = db.collection(this.entity);
        const snapshot = await placeCol
            .where('registeredBy', '==', registeredBy)
            .where('status', 'in', [Constants.PLACE_STATUS.INCOMPLETE, Constants.PLACE_STATUS.WAITING, Constants.PLACE_STATUS.REJECTED])
            .select().get();
        const batch = db.batch();
        snapshot.forEach((doc: any) => {
            const placeRef = placeCol.doc(doc.id);
            batch.delete(placeRef);
        });
        await batch.commit();
    }

    static async updateLocation(id: string, place: PlaceResponse): Promise<any> {
        const geofirestore: GeoFirestore = new GeoFirestore(db);
        const geocollection: GeoCollectionReference = geofirestore.collection(this.entity);
        await geocollection.doc(id).update({
            coordinates: new admin.firestore.GeoPoint(place.latitude, place.longitude),
        });
        await db.collection(this.entity).doc(id).update({
            location: place.location,
            latitude: place.latitude,
            longitude: place.longitude,
        });
    }

    private static concatFlyer(flyer: PlaceFlyerDto): string {
        let text: any = '';
        text = flyer.title;
        for (const item of flyer.textList) {
            text = text.concat(`, ${item}`);
        }
        return text;
    }

    static async deleteFlyer(id: string) {
        await db.collection(this.entity).doc(id).update({
            flyer: admin.firestore.FieldValue.delete(),
            lastUpdate: admin.firestore.FieldValue.serverTimestamp()
        });
        // await new GeoFirestore(db).collection(this.entity).doc(id).update({
        //     flyer: admin.firestore.FieldValue.delete()
        // });
    }

    static async updateSearchParamsAndStatusAndCategoryAndType(placeId: string): Promise<any> {
        const place: PlaceResponse = await this.findById(placeId);
        let searchText = place.name;
        searchText = `${searchText}, ${place.type}`;
        if (place.flyer) {
            searchText = `${searchText}, ${this.concatFlyer(place.flyer)}`;
        }
        if (place.category) {
            searchText = `${searchText}, ${place.category.name}`.replace('(Otros)', '');
        }
        if (place.location) {
            searchText = `${searchText}, ${place.location.address}`;
            searchText = `${searchText}, ${place.location.addressLine}`;
        }
        searchText = searchText.toLowerCase();
        const data: any = {
            searchParams: searchText,
            category: place.category?.name,
            status: place.status
        };
        place.type ? data.type = place.type : delete data.type;
        await new GeoFirestore(db).collection(this.entity).doc(placeId).update(data);
    }
}
