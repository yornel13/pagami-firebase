import * as admin from 'firebase-admin';
import { PlacesRepository } from './places.repository';
import { Constants } from '../../../contants/contants';
import { PlaceIncompleteDto } from "./domain/dto/place.incomplete.dto";
import { ServerResponse } from "../base/server.response";
import { PlaceUnregisterDto } from "./domain/dto/place.unregister.dto";
import { Mappers } from "../../mappers/mappers";
import { ClaimStatus, PlaceResponse } from "./domain/response/place.response";
import { PlaceFilterDto } from "./domain/dto/place.filter.dto";
import { CountPlaces } from "./domain/response/count.places";
import { UserResponse } from '../users/domain/response/user.response';
import { UsersService } from "../users/users.service";
import { NotificationsService } from "../notifications/notifications.service";
import { ClaimsService } from '../claims/claims.service';
import { ClaimDto } from '../claims/domain/dto/claim.dto';
import { ProductsService } from '../products/products.service';
import { PlaceCategoryDto } from './domain/dto/place.category.dto';
import { PlaceFlyerDto } from './domain/dto/place.flyer.dto';
import { PlaceHoursDto } from "./domain/dto/place.hours.dto";
import { PlacePlanDto } from './domain/dto/place.plan.dto';

const repository = PlacesRepository;

export class PlacesService {

    /**
     * save a place with only coors
     * this method create place with status INCOMPLETE
     */
    static async save(placeDto: PlaceIncompleteDto): Promise<ServerResponse> {
        const newRecordsToday = await this.validUserTodayRecords(placeDto.registeredBy!);
        if (newRecordsToday) {
            placeDto.createTime = admin.firestore.FieldValue.serverTimestamp();
            placeDto.lastUpdate = admin.firestore.FieldValue.serverTimestamp();
            placeDto.status = Constants.PLACE_STATUS.INCOMPLETE;
            const id: string = await repository.save(placeDto);
            const placeResponse = await repository.findById(id);
            await UsersService.updateTodayRecords(placeDto.registeredBy!, newRecordsToday);
            return Mappers.responsePassed(placeResponse);
        } else {
            return Mappers.response(
                Constants.RESPONSE.CANT_REGISTER_PLACE_TODAY,
                false,
                Constants.RESPONSE.CODE.CANT_REGISTER_PLACE_TODAY
            );
        }
    }

    private static async validUserTodayRecords(userId: string) {
        const user: UserResponse = (await UsersService.findById(userId)).response;
        if (user) {
            if (user.recordsToday) {
                const d1 = user.recordsToday.day.toDate();
                const d2 = new Date();
                const sameDay = d1.getFullYear() === d2.getFullYear() &&
                    d1.getMonth() === d2.getMonth() &&
                    d1.getDate() === d2.getDate();
                if (sameDay) {
                    if (user.recordsToday.count >= Constants.PLACE.MAXIMUM_DAILY_RECORDS) {
                        return false;
                    } else {
                        user.recordsToday.count = Number(user.recordsToday.count) + 1;
                    }
                } else {
                    user.recordsToday = {
                        count: 1,
                        day: admin.firestore.FieldValue.serverTimestamp()
                    };
                }
            } else {
                user.recordsToday = {
                    count: 1,
                    day: admin.firestore.FieldValue.serverTimestamp()
                };
            }
            return user.recordsToday;
        } else {
            return false;
        }
    }

    /**
     * update a place
     * this method update place status to WAITING if current is INCOMPLETE
     * this method return error if place status is different that INCOMPLETE or WAITING
     */
    static async update(userId: string, placeId: string, placeDto: any): Promise<ServerResponse> {
        const currentPlace: PlaceResponse = await repository.findById(placeId);
        if (currentPlace.status === Constants.PLACE_STATUS.INCOMPLETE
            || currentPlace.status === Constants.PLACE_STATUS.WAITING) {
            if (userId === currentPlace.registeredBy || await UsersService.isAdmin(userId)) {
                return this.updateNoAccepted(placeId, placeDto);
            } else {
                return Mappers.response(Constants.RESPONSE.HAS_NO_PRIVILEGES);
            }
        } else if (currentPlace.status === Constants.PLACE_STATUS.VERIFIED) {
            if (/*validate if owner*/true || await UsersService.isAdmin(userId)) {
                return this.updateVerified(placeId, placeDto);
            } else {
                return Mappers.response(Constants.RESPONSE.HAS_NO_PRIVILEGES);
            }
        } else {
            return Mappers.response(Constants.RESPONSE.NOT_FOUND);
        }
    }

    /**
     * update a place
     * this method update place status to WAITING if current is INCOMPLETE
     * this method return error if place status is different that INCOMPLETE or WAITING
     */
    private static async updateNoAccepted(placeId: string, placeDto: PlaceUnregisterDto): Promise<ServerResponse> {
        if (placeDto.status === Constants.PLACE_STATUS.INCOMPLETE) {
            placeDto.status = Constants.PLACE_STATUS.WAITING;
        } else {
            delete placeDto.status;
        }
        placeDto.lastUpdate = admin.firestore.FieldValue.serverTimestamp();
        await repository.update(placeId, placeDto);
        await repository.updateSearchParamsAndStatusAndCategoryAndType(placeId);
        const placeResponse: PlaceResponse = await repository.findById(placeId);
        return Mappers.responsePassed(placeResponse);
    }

    /**
     * update a place
     * this method update a place with status VERIFIED
     */
    private static async updateVerified(placeId: string, placeDto: PlaceUnregisterDto): Promise<ServerResponse> {
        const object = {
            photoUrl: placeDto.photoUrl,
            phone: placeDto.phone,
            samePhone: placeDto.samePhone,
            whatsapp: placeDto.whatsapp,
            website: placeDto.website,
            name: placeDto.name,
            location: placeDto.location,
            lastUpdate: admin.firestore.FieldValue.serverTimestamp(),
            allowHome: placeDto.allowHome
        };
        await repository.update(placeId, object);
        await repository.updateSearchParamsAndStatusAndCategoryAndType(placeId);
        const placeResponse: PlaceResponse = await repository.findById(placeId);
        return Mappers.responsePassed(placeResponse);
    }

    /**
     * make a place to accepted status
     * this method change status on a waiting place to accepted
     * can only execute by an admin
     */
    static async makeAccepted(uid: string, placeId: string): Promise<ServerResponse> {
        if (await UsersService.isAdmin(uid)) {
            let place: PlaceResponse = await repository.findById(placeId);
            if (place.status === Constants.PLACE_STATUS.WAITING) {
                const update = {
                    status: Constants.PLACE_STATUS.ACCEPTED,
                    lastUpdate: admin.firestore.FieldValue.serverTimestamp()
                };
                await repository.update(placeId, update);
                await repository.updateSearchParamsAndStatusAndCategoryAndType(placeId);
                place = await repository.findById(placeId);
                await UsersService.increaseUserRegisteredPlaces(place.registeredBy!);
                await NotificationsService.createNotificationPlaceAccepted(place.registeredBy!, placeId);
                return Mappers.responsePassed(place);
            } else {
                return Mappers.response(Constants.RESPONSE.PLACE_IS_NOT_WAITING);
            }
        } else {
            return Mappers.response(Constants.RESPONSE.HAS_NO_PRIVILEGES);
        }
    }

    /**
     * make a place to reject status
     * this method change status on a waiting place to reject
     * can only execute by an admin
     */
    static async makeRejected(uid: string, placeId: string, reason: string): Promise<ServerResponse> {
        if (await UsersService.isAdmin(uid)) {
            let place: PlaceResponse = await repository.findById(placeId);
            if (place.status === Constants.PLACE_STATUS.WAITING) {
                const update = {
                    rejectedReason: reason,
                    status: Constants.PLACE_STATUS.REJECTED,
                    lastUpdate: admin.firestore.FieldValue.serverTimestamp()
                };
                await repository.update(placeId, update);
                await repository.updateSearchParamsAndStatusAndCategoryAndType(placeId);
                place = await repository.findById(placeId);
                await NotificationsService.createNotificationPlaceRejected(place.registeredBy!, placeId);
                return Mappers.responsePassed(place);
            } else {
                return Mappers.response(Constants.RESPONSE.PLACE_IS_NOT_WAITING);
            }
        } else {
            return Mappers.response(Constants.RESPONSE.HAS_NO_PRIVILEGES);
        }
    }

    /**
     * make a place to verified status
     * this method change status on a accepted place to verified
     */
    static async makeVerified(placeId: string): Promise<ServerResponse> {
        const place: PlaceResponse = await repository.findById(placeId);
        if (place && place.status === Constants.PLACE_STATUS.ACCEPTED) {
            place.claim!.status = Constants.CLAIM_STATUS.ACCEPTED;
            const update = {
                status: Constants.PLACE_STATUS.VERIFIED,
                lastUpdate: admin.firestore.FieldValue.serverTimestamp(),
                claim: place.claim
            };
            await repository.update(placeId, update);
            await repository.updateSearchParamsAndStatusAndCategoryAndType(placeId);
            return Mappers.responsePassed(place);
        } else {
            return Mappers.response(Constants.RESPONSE.PLACE_NOT_AVAILABLE);
        }
    }

    /**
     * make a place to accepted status
     * this method change status on a waiting place to accepted
     * can only execute by an admin
     */
    static async clearClaim(placeId: string): Promise<any> {
        const place: PlaceResponse = await repository.findById(placeId);
        if (place && (place.status === Constants.PLACE_STATUS.ACCEPTED || place.status === Constants.PLACE_STATUS.VERIFIED)) {
            const update = {
                lastUpdate: admin.firestore.FieldValue.serverTimestamp(),
                claim: admin.firestore.FieldValue.delete()
            };
            await repository.update(placeId, update);
        } else {
            throw Constants.RESPONSE.PLACE_NOT_AVAILABLE;
        }
    }

    /**
     * find place by id
     * this method return a place by id
     */
    static async findById(id: string): Promise<ServerResponse> {
        const object = await repository.findById(id);
        if (object !== undefined && object !== null) {
            return Mappers.responsePassed(object);
        } else {
            return Mappers.response(Constants.RESPONSE.NOT_FOUND);
        }
    }

    /**
     * get all places
     * this method return all places
     */
    static async getAll(): Promise<any> {
        const places = await repository.getAll();
        return Mappers.responsePassed(places);
    }

    /**
     * get all places by available
     * this method return all available places (VERIFIED & ACCEPTED)
     */
    static async getAllAvailable(): Promise<ServerResponse> {
        const objects = await repository.getAllAvailable();
        return Mappers.responsePassed(objects);
    }

    /**
     * get all places with a select query
     * this method return all available places (VERIFIED & ACCEPTED)
     */
    static async getAllAvailableWithSelect(): Promise<ServerResponse> {
        const objects = await repository.getAllAvailableWithSelect();
        return Mappers.responsePassed(objects);
    }

    /**
     * get all places by status
     * this method return all places by status
     */
    static async getAllByStatus(status: string): Promise<ServerResponse> {
        const objects = await repository.getByStatus(status);
        return Mappers.responsePassed(objects);
    }

    /**
     * get all places by status claim waiting
     * this method return all places by claim
     */
    static async getAllWithStatusClaimWaiting(): Promise<ServerResponse> {
        const objects = await repository.getWithStatusClaimWaiting();
        return Mappers.responsePassed(objects);
    }

    /**
     * get all places by user id
     * this method return all places registered by a user
     */
    static async getByRegistered(userId: string): Promise<ServerResponse> {
        const objects = await repository.getByRegistered(userId);
        return Mappers.responsePassed(objects);
    }

    /**
     * get all places by user id
     * this method return all places registered by a user
     */
    static async getNearby(filterDto: PlaceFilterDto): Promise<ServerResponse> {
        const objects = filterDto.text
            ? await this.getNearbyFilterText(filterDto)
            : await repository.getNearby(filterDto);
        return Mappers.responsePassed(objects);
    }

    static async getNearbyFilterText(filterDto: PlaceFilterDto): Promise<any> {
        const placesAll: any[] = await repository.getNearbyFilterText(filterDto);
        const filterPlaces = placesAll.filter(place => {
            return place.d && place.d.searchParams && place.d.searchParams.toLowerCase().includes(filterDto.text!.toLowerCase());
        });
        filterPlaces.forEach(place => {
            delete place.d;
        });
        return filterPlaces;
    }

    /**
     * delete place by id
     * this method delete a place by id if that place is in INCOMPLETE or WAITING status
     */
    static async delete(id: string): Promise<any> {
        const currentPlace = await repository.findById(id);
        if (currentPlace.status === Constants.PLACE_STATUS.INCOMPLETE
            || currentPlace.status === Constants.PLACE_STATUS.WAITING) {
            await repository.delete(id);
            return Mappers.responsePassed(Constants.RESPONSE.OBJECT_DELETED);
        } else {
            return Mappers.response(Constants.RESPONSE.CANT_DELETED);
        }
    }

    /**
     * save claim status
     */
    static async claim(id: string, claim: ClaimStatus): Promise<any> {
        await repository.claim(id, claim);
    }

    /**
     * count places states
     */
    static async countPlaces() {
        const countData: CountPlaces = {
            verified: await repository.countVerified(),
            accepted: await repository.countAccepted(),
            total: await repository.countTotal(),
            expired: await repository.countExpired()
        };
        return Mappers.responsePassed(countData);
    }

    /**
     * delete places status
     */
    static async deleteAllIncompleteOrWaitingByUser(registeredBy: string): Promise<any> {
        await repository.deleteAllIncompleteOrWaitingByUser(registeredBy);
    }

    /**
     * update place location
     * this method update a place location by owner or a admin
     */
    static async updateLocation(userId: string, placeId: string, placeDto: any): Promise<ServerResponse> {
        const claimDto: ClaimDto = (await ClaimsService.getByUserIdIfWaitingOrAccepted(userId)).response;
        const fromUser = (claimDto && claimDto.status === Constants.PLACE_STATUS.ACCEPTED && claimDto.placeId === placeId);
        if (await UsersService.isAdmin(userId) || fromUser) {
            await repository.updateLocation(placeId, placeDto);
            await repository.updateSearchParamsAndStatusAndCategoryAndType(placeId);
            if (fromUser) {
                return ClaimsService.getByUserIdIfWaitingOrAccepted(userId);
            } else {
                return PlacesService.findById(placeId);
            }
        } else {
            return Mappers.response(Constants.RESPONSE.NOT_FOUND);
        }
    }

    /**
     * valid place status
     * this method update a place location by owner or a admin
     */
    static async validPlaceStatus(placeId: string, status: string): Promise<boolean> {
        const place = await PlacesRepository.findById(placeId);
        if (!place) {
            return false;
        }
        return place.status === status;
    }

    /**
     * delete place by id
     * this method delete a place and their associations, only available if user is admin
     */
    static async deleteFull(userId: string, id: string): Promise<any> {
        if (await UsersService.isAdmin(userId)) {
            if (await repository.exist(id)) {
                await ProductsService.deleteByPlaceId(id);
                await ClaimsService.deleteByPlaceId(id);
                await repository.delete(id);
                return Mappers.responsePassed(Constants.RESPONSE.OBJECT_DELETED);
            } else {
                return Mappers.response(Constants.RESPONSE.CANT_DELETED);
            }
        } else {
            return Mappers.response(Constants.RESPONSE.HAS_NO_PRIVILEGES);
        }
    }

    /**
     * update a place category
     */
    static async updateCategory(placeId: string, categoryDto: PlaceCategoryDto): Promise<ServerResponse> {
        categoryDto.lastUpdate = admin.firestore.FieldValue.serverTimestamp();
        await repository.update(placeId, categoryDto);
        await repository.updateSearchParamsAndStatusAndCategoryAndType(placeId);
        const placeResponse: PlaceResponse = await repository.findById(placeId);
        return Mappers.responsePassed(placeResponse);
    }

    /**
     * update a place flyer
     */
    static async updateFlyer(placeId: string, flyer: PlaceFlyerDto): Promise<ServerResponse> {
        await repository.update(placeId, { flyer, lastUpdate: admin.firestore.FieldValue.serverTimestamp() });
        await repository.updateSearchParamsAndStatusAndCategoryAndType(placeId);
        const placeResponse: PlaceResponse = await repository.findById(placeId);
        return Mappers.responsePassed(placeResponse);
    }

    /**
     * delete a place flyer
     */
    static async deleteFlyer(placeId: string): Promise<ServerResponse> {
        await repository.deleteFlyer(placeId);
        await repository.updateSearchParamsAndStatusAndCategoryAndType(placeId);
        const placeResponse: PlaceResponse = await repository.findById(placeId);

        return Mappers.responsePassed(placeResponse);
    }

    /**
     * update a place hours
     */
    static async updateHours(placeId: string, hours: PlaceHoursDto): Promise<ServerResponse> {
        await repository.update(placeId, { hours, lastUpdate: admin.firestore.FieldValue.serverTimestamp() });
        const placeResponse: PlaceResponse = await repository.findById(placeId);
        return Mappers.responsePassed(placeResponse);
    }

    /**
     * delete a place hours
     */
    static async deleteHours(placeId: string): Promise<ServerResponse> {
        await repository.update(placeId, { hours: undefined, lastUpdate: admin.firestore.FieldValue.serverTimestamp() });
        const placeResponse: PlaceResponse = await repository.findById(placeId);
        return Mappers.responsePassed(placeResponse);
    }

    static async updateAllPlacesSearchParams() {
        const places: PlaceResponse[] = await repository.getAllAvailable();
        for (const place of places) {
            const id: any = place.id;
            await repository.updateSearchParamsAndStatusAndCategoryAndType(id);
        }
        return Mappers.responsePassed(Constants.RESPONSE.SUCCESS);
    }

    /**
     * update a place plan
     * @returnPlaceUpdated: return the place is true
     */
    static async updatePlan(placeId: string, plan: PlacePlanDto, returnPlaceUpdated = false): Promise<any> {
        await repository.update(placeId, { plan, lastUpdate: admin.firestore.FieldValue.serverTimestamp() });
        if (returnPlaceUpdated) {
            return await repository.findById(placeId);
        }
    }
}
