import * as express from 'express';
import * as asyncHandler from 'express-async-handler';
import { CurrenciesController } from './currencies.controller';
const basePath = '/currencies';
const app = express();
const controller = CurrenciesController;

app.get(`${basePath}`, asyncHandler(async (req, res, next) => {
    await controller.getAllActive(req, res, next);
}));

export const currenciesRoute = app;
