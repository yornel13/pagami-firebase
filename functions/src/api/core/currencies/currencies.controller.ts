import { Request, Response, NextFunction } from "express";
import { Constants } from "../../../contants/contants";
import { CurrenciesService } from './currencies.service';

const service = CurrenciesService;

export class CurrenciesController {

    static async getAllActive(req: Request, res: Response, next: NextFunction) {
        try {
            const response = await service.getAllActive();
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }
}
