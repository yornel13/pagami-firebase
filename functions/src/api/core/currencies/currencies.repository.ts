import { BaseRepository } from '../base/base.repository';
import { db } from '../../../index';

export class CurrenciesRepository extends BaseRepository {

    static entity = 'currencies';

    static async getAllActive(): Promise<any> {
        const snapshot = await db.collection(this.entity).where('active', '==', true).get();
        const list: any[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        return list;
    }
}
