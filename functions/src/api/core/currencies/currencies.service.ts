import { Mappers } from '../../mappers/mappers';
import { ServerResponse } from '../base/server.response';
import { CurrenciesRepository } from './currencies.repository';

const repository = CurrenciesRepository;

export class CurrenciesService {

    /**
     * get all currencies actives
     */
    static async getAllActive(): Promise<ServerResponse> {
        const objects = await repository.getAllActive();
        return Mappers.responsePassed(objects);
    }
}
