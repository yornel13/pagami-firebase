import { ConfigRepository } from './config.repository';
import { ServerResponse } from '../base/server.response';
import { Mappers } from '../../mappers/mappers';

const repository = ConfigRepository;

export class ConfigService {

    /**
     * get all active payment methods
     * this method return all plans active
     */
    static async getPaymentMethods(): Promise<ServerResponse> {
        const objects = await repository.getPaymentMethods();
        return Mappers.responsePassed(objects);
    }
}
