import * as express from 'express';
import * as asyncHandler from 'express-async-handler';
import { ConfigController } from './config.controller';

const basePath = '/config';
const app = express();
const controller = ConfigController;

app.get(`${basePath}/payment-methods`, asyncHandler(async (req, res, next) => {
    await controller.getPaymentMethods(req, res, next);
}));

export const configRoute = app;
