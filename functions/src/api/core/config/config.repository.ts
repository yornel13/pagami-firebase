import {BaseRepository} from '../base/base.repository';
import {db} from '../../../index';

export class ConfigRepository extends BaseRepository {

    static entity = 'config';
    static payMethod = 'payMethods';

    static async getPaymentMethods() {
        const doc = await db.collection(this.entity).doc(this.payMethod).get();
        if (doc.exists) {
            return this.mapDocToObject(doc);
        } else {
            return null;
        }
    }
}
