import { Request, Response, NextFunction } from "express";
import { Constants } from "../../../contants/contants";
import { ConfigService } from './config.service';

const service = ConfigService;

export class ConfigController {

    static async getPaymentMethods(req: Request, res: Response, next: NextFunction) {
        try {
            const response = await service.getPaymentMethods();
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    };
}
