export interface ServerResponse {
    response?: any,
    passed: boolean,
    code?: number,
    message?: string
}
