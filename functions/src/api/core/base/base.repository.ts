import { db } from '../../../index';
import { Utils } from '../../../utils/utils';

export abstract class BaseRepository {

    static entity = '';

    static mapDocToObject(doc: any) {
        const object = doc.data();
        object.id = doc.id;
        try {
            if (object.createTime) {
                object.createTime = object.createTime.toDate();
            }
            if (object.lastUpdate) {
                object.lastUpdate = object.lastUpdate.toDate();
            }
        } catch (e) { }
        return object;
    }

    static async getAll(): Promise<any> {
        const snapshot = await db.collection(this.entity).get();
        const list: any[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        return list;
    }

    static async findById(id: string): Promise<any> {
        const doc = await db.collection(this.entity).doc(id).get();
        if (doc.exists) {
            return this.mapDocToObject(doc);
        } else {
            return null;
        }
    }

    static async save(object: any): Promise<string> {
        delete object.id;
        Utils.removeUndefinedFields(object);
        const objectRef = await db.collection(this.entity).add(object);
        return objectRef.id;
    }

    static async saveAll(list: any[]): Promise<any> {
        const batch = db.batch();
        const ids: any[] = [];
        list.forEach(object => {
            const objectRef = db.collection(this.entity).doc();
            batch.set(objectRef, object);
            ids.push(objectRef.id);
        });

        await batch.commit();
        return ids;
    }

    static async update(id: string, object: any): Promise<any> {
        delete object.id;
        delete object.createTime;
        Utils.addFirebaseDeleteToUndefinedFields(object);
        await db.collection(this.entity).doc(id).update(object);
        return id;
    }

    static async delete(id: string): Promise<any> {
        await db.collection(this.entity).doc(id).delete();
        return true;
    }

    static async exist(id: string): Promise<boolean>{
        const doc = await db.collection(this.entity).doc(id).get();
        return doc.exists;
    }
}
