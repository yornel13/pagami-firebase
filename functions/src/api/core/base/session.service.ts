import * as admin from 'firebase-admin';
import { Constants } from '../../../contants/contants';
import { Response, Request } from 'express';
import { Mappers } from "../../mappers/mappers";

export class SessionService {

    static async verify(req: Request, res: Response) {
        try {
            const token: any = req.headers.authorization;
            const auth = await admin.auth().verifyIdToken(token);
            if (auth !== null) {
                req.headers.uid = auth.uid;
                return true;
            } else {
                res.status(Constants.HTTP_STATUS.OK).json(
                    Mappers.response(
                        Constants.RESPONSE.USER_NOT_REGISTERED,
                        false,
                        Constants.RESPONSE.CODE.NOT_REGISTERED
                    )
                );
                return true;
            }
        } catch (err) {
            if (err.code === Constants.ADMIN_AUTH.RESPONSE_CODES.TOKEN_EXPIRED) {
                res.status(Constants.HTTP_STATUS.UNAUTHORIZED).json(
                    Mappers.response(
                        Constants.RESPONSE.SESSION_EXPIRED,
                        false,
                        Constants.RESPONSE.CODE.SESSION_EXPIRED
                    )
                );
            } else {
                res.status(Constants.HTTP_STATUS.UNAUTHORIZED).json(
                    Mappers.response(
                        Constants.RESPONSE.SESSION_INVALID,
                        false,
                        Constants.RESPONSE.CODE.SESSION_INVALID
                    )
                );
            }
            return false;
        }
    }
}
