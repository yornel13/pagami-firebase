export interface NotificationDto {
    message: string;
    read: boolean;
    placeId: string;
    extra?: any;
    createTime: any;
    lastUpdate: any;
    type: string;
    deleted: boolean;
}
