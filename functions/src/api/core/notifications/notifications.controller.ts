import { Request, Response, NextFunction } from "express";
import { Constants } from "../../../contants/contants";
import { NotificationsService } from './notifications.service';

const service = NotificationsService;

export class NotificationsController {

    static async getAllUnreadByUser(req: Request, res: Response, next: NextFunction) {
        try {
            const uid: any = req.headers.uid;
            const response = await service.getAllUnreadByUser(uid);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    };

    static async putReadAllAcceptedAndRejected(req: Request, res: Response, next: NextFunction) {
        try {
            const uid: any = req.headers.uid;
            const response = await service.putReadAllAcceptedAndRejected(uid);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    };
}
