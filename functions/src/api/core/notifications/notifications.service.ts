import * as admin from 'firebase-admin';
import { NotificationsRepository } from './notifications.repository';
import { Constants } from '../../../contants/contants';
import { Mappers } from '../../mappers/mappers';

const repository = NotificationsRepository;

export class NotificationsService {

    static async createNotificationPlaceAccepted(userId: string, placeId: string) {
        const notification = {
            type: Constants.NOTIFICATIONS.TYPE.PLACE_ACCEPTED,
            message: Constants.NOTIFICATIONS.MESSAGES.PLACE_ACCEPTED,
            placeId,
        };
        await this.createNotification(userId, notification);
    }

    static async createNotificationPlaceRejected(userId: string, placeId: string) {
        const notification = {
            type: Constants.NOTIFICATIONS.TYPE.PLACE_REJECTED,
            message: Constants.NOTIFICATIONS.MESSAGES.PLACE_REJECTED,
            placeId,
        };
        await this.createNotification(userId, notification);
    }

    static async createNotificationPaymentAccepted(userId: string, placeId: string, paymentId: string) {
        const notification = {
            type: Constants.NOTIFICATIONS.TYPE.PAYMENT_ACCEPTED,
            message: Constants.NOTIFICATIONS.MESSAGES.PAYMENT_ACCEPTED,
            placeId,
            paymentId
        };
        await this.createNotification(userId, notification);
    }

    static async createNotificationPaymentRejected(userId: string, placeId: string, paymentId: string) {
        const notification = {
            type: Constants.NOTIFICATIONS.TYPE.PAYMENT_REJECTED,
            message: Constants.NOTIFICATIONS.MESSAGES.PAYMENT_REJECTED,
            placeId,
            paymentId
        };
        await this.createNotification(userId, notification);
    }

    /**
     * create a notifications to a user
     * return none
     */
    private static async createNotification(userId: string, notification: any) {
        notification.createTime = admin.firestore.FieldValue.serverTimestamp();
        notification.lastUpdate = admin.firestore.FieldValue.serverTimestamp();
        notification.read = false;
        notification.deleted = false;
        await repository.save(userId, notification);
    }

    static async getAllUnreadByUser(userId: string): Promise<any> {
        const notifications = await repository.getAllUnreadByUser(userId);
        return Mappers.responsePassed(notifications);
    }

    static async putReadAllAcceptedAndRejected(userId: string): Promise<any> {
        await repository.putReadAllAcceptedAndRejected(userId);
        return Mappers.responsePassed(Constants.RESPONSE.SUCCESS);
    }
    // /**
    //  * get all places
    //  * this method return all places
    //  */
    // static async getAll(): Promise<any> {
    //     const places = await repository.getAll();
    //     return Mappers.responsePassed(places);
    // }
}
