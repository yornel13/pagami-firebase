import * as express from 'express';
import * as asyncHandler from 'express-async-handler';
import { NotificationsController } from './notifications.controller';

const basePath = '/notifications';
const app = express();
const controller = NotificationsController;

app.get(`${basePath}`, asyncHandler(async (req, res, next) => {
    await controller.getAllUnreadByUser(req, res, next);
}));

app.put(`${basePath}/read/all-accepted-rejected`, asyncHandler(async (req, res, next) => {
    await controller.putReadAllAcceptedAndRejected(req, res, next);
}));

export const notificationsRoute = app;
