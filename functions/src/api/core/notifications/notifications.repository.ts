import { db } from '../../../index';
import { UsersRepository } from '../users/users.repository';
import * as admin from 'firebase-admin';
import { Constants } from '../../../contants/contants';

export class NotificationsRepository {

    static entity = 'notifications';

    static mapDocToObject(doc: any) {
        const object = doc.data();
        try {
            if (object.createTime) {
                object.createTime = object.createTime.toDate();
            }
            if (object.lastUpdate) {
                object.lastUpdate = object.lastUpdate.toDate();
            }
        } catch (e) { }
        return object;
    }

    static async save(userId: string, object: any): Promise<string> {
        delete object.id;
        const objectRef = await db.collection(UsersRepository.entity).doc(userId).collection(this.entity).add(object);
        return objectRef.id;
    }

    static async getAll(userId: string): Promise<any> {
        const snapshot = await db.collection(UsersRepository.entity).doc(userId).collection(this.entity).get();
        const list: any[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        return list;
    }

    static async getAllUnreadByUser(userId: string): Promise<any> {
        const snapshot = await db.collection(UsersRepository.entity).doc(userId).collection(this.entity)
            .where('read', '==', false).get();
        const list: any[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        return list;
    }

    static async putReadAllAcceptedAndRejected(userId: string): Promise<any> {
        const notificationRef = db.collection(UsersRepository.entity).doc(userId).collection(this.entity);
        const snapshot = await notificationRef
            .where('read', '==', false)
            .where('type', 'in', [
                Constants.NOTIFICATIONS.TYPE.PLACE_ACCEPTED,
                Constants.NOTIFICATIONS.TYPE.PLACE_REJECTED,
                Constants.NOTIFICATIONS.TYPE.PAYMENT_REJECTED,
                Constants.NOTIFICATIONS.TYPE.PAYMENT_ACCEPTED
            ])
            .select().get();
        const batch = db.batch();
        snapshot.forEach((doc: any) => {
            const docRef = notificationRef.doc(doc.id);
            batch.update(docRef, { read: true });
        });
        await batch.commit();
    }

    static async putReadAllByType(userId: string, type: string): Promise<any> {
        const notificationRef = db.collection(UsersRepository.entity).doc(userId).collection(this.entity);
        const snapshot = await notificationRef
            .where('read', '==', false)
            .where('type', '==', type)
            .select().get();
        const batch = db.batch();
        snapshot.forEach((doc: any) => {
            const docRef = notificationRef.doc(doc.id);
            batch.update(docRef, { read: true });
        });
        await batch.commit();
    }

    static async putRead(userId: string, id: string): Promise<any> {
        await db.collection(UsersRepository.entity)
            .doc(userId)
            .collection(this.entity)
            .doc(id)
            .update({ read: true , lastUpdate: admin.firestore.FieldValue.serverTimestamp() });
    }
}
