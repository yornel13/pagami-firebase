import { Request, Response, NextFunction } from "express";
import { Constants } from "../../../contants/contants";
import { PlansService } from './plans.service';

const service = PlansService;

export class PlansController {

    static async getActive(req: Request, res: Response, next: NextFunction) {
        try {
            const response = await service.getActive();
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async findById(req: Request, res: Response, next: NextFunction) {
        try {
            const id = req.params.id;
            const response = await service.findById(id);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }
}
