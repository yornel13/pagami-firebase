import { PlansRepository } from './plans.repository';
import { ServerResponse } from '../base/server.response';
import { Mappers } from '../../mappers/mappers';
import { PlacePlanDto } from '../places/domain/dto/place.plan.dto';
import * as admin from 'firebase-admin';
import { PlacesService } from '../places/places.service';
import { PlaceResponse } from '../places/domain/response/place.response';

const repository = PlansRepository;

export class PlansService {

    /**
     * get all plans active
     * this method return all plans active
     */
    static async getActive(): Promise<ServerResponse> {
        const objects = await repository.getActive();
        return Mappers.responsePassed(objects);
    }

    /**
     * get plan by id
     * this method return a plan
     */
    static async findById(id: string): Promise<ServerResponse> {
        const plan =  await repository.findById(id);
        return Mappers.responsePassed(plan);
    }

    static async calculatePlacePlanRemain(place: PlaceResponse): Promise<any> {
        const plan: any = place.plan;
        if (plan && plan.isActive) {
            const diff = Math.abs(new Date().setHours(0,0,0,0)) - plan.lastCheckDate.setHours(0,0,0,0);
            const diffInDays = Math.ceil(diff / (1000 * 3600 * 24));
            if (diffInDays > 0) {
                const currentDays = plan.remainDays || 0;
                const newDaysLeft = currentDays - diffInDays;
                plan.remainDays = newDaysLeft < 0 ? 0 : newDaysLeft;
                plan.lastCheckDate = new Date();
                plan.isActive = newDaysLeft > 0;
                if (place.id != null) {
                    await PlansService.updatePlacePlan(place.id, plan);
                }
            }
        }
    }

    /**
     * Update a place plan by place id
     * this method return the full place
     */
    static async updatePlacePlan(placeId: string, plan: PlacePlanDto, returnPlaceUpdated = false): Promise<any> {
        const updatedPlan: PlacePlanDto = {};
        updatedPlan.limitProducts = plan.limitProducts;
        updatedPlan.limitServices = plan.limitServices;
        updatedPlan.remainDays = plan.remainDays;
        updatedPlan.activateDate = admin.firestore.Timestamp.fromDate(plan.activateDate);
        updatedPlan.lastCheckDate = admin.firestore.Timestamp.fromDate(plan.lastCheckDate);
        updatedPlan.isActive = plan.isActive;
        updatedPlan.planId = plan.planId;
        return await PlacesService.updatePlan(placeId, updatedPlan, returnPlaceUpdated);
    }
}
