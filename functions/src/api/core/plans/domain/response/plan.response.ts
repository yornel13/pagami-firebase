export interface PlanResponse {
    id: string;
    name: string;
    amount: number;
    days: number;
    daysText: string;
    daysSubText: string;
    promotionText?: string;
    active: boolean;
    maxProducts: number;
    maxServices: number;
    amountCop?: number;
}
