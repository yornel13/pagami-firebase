import * as express from 'express';
import * as asyncHandler from 'express-async-handler';
import { PlansController } from './plans.controller';

const basePath = '/plans';
const app = express();
const controller = PlansController;

app.get(`${basePath}`, asyncHandler(async (req, res, next) => {
    await controller.getActive(req, res, next);
}));

app.get(`${basePath}/:id`, asyncHandler(async (req, res, next)  => {
    await controller.findById(req, res, next);
}));

export const plansRoute = app;
