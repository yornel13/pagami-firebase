import { BaseRepository } from '../base/base.repository';
import { db } from '../../../index';

export class ProductsRepository extends BaseRepository {

    static entity = 'products';

    static async findByPlaceId(placeId: string): Promise<any> {
        const snapshot = await db.collection(this.entity).where('placeId', '==', placeId).get();
        const list: any[] = [];
        snapshot.forEach((doc: any) => {
            const object = this.mapDocToObject(doc);
            list.push(object);
        });
        return list;
    }

    static async deleteByPlaceId(placeId: string) {
        const productsCollection = db.collection(this.entity);
        const snapshot = await productsCollection
            .where('placeId', '==', placeId)
            .select().get();
        const batch = db.batch();
        snapshot.forEach((doc: any) => {
            const claimRef = productsCollection.doc(doc.id);
            batch.delete(claimRef);
        });
        await batch.commit();
    }
}
