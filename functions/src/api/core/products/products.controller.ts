import { Request, Response, NextFunction } from "express";
import { Constants } from "../../../contants/contants";
import { ProductsService } from './products.service';
import { ProductDto } from './domain/dto/product.dto';
import { ProductValidation } from '../../validations/product.validation';

const service = ProductsService;

export class ProductsController {

    static async save(req: Request, res: Response, next: NextFunction) {
        try {
            if (ProductValidation.isValid(req, res)) {
                const productDto: ProductDto = req.body;
                const uid: any = req.headers.uid;
                productDto.registeredBy = uid;
                const response = await service.save(productDto);
                res.status(Constants.HTTP_STATUS.OK).json(response);
            }
        } catch (e) {
            next(e);
        }
    };

    static async updateStatus(req: Request, res: Response, next: NextFunction) {
        try {
            const status: boolean = Boolean(JSON.parse(req.params.status));
            const productId: string = req.params.id;
            const response = await service.changeStatus(productId, status);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    };

    static async updateStock(req: Request, res: Response, next: NextFunction) {
        try {
            const stock: number = Number(req.params.stock);
            const productId: string = req.params.id;
            const response = await service.changeStock(productId, stock);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    };

    static async update(req: Request, res: Response, next: NextFunction) {
        try {
            if (ProductValidation.isValid(req, res, true)) {
                const productDto: ProductDto = req.body;
                const productId: string = req.params.id;
                productDto.id = productId;
                const response = await service.update(productDto);
                res.status(Constants.HTTP_STATUS.OK).json(response);
            }
        } catch (e) {
            next(e);
        }
    };

    static async getByPlaceId(req: Request, res: Response, next: NextFunction) {
        try {
            const placeId: string = req.params.placeId;
            const response = await service.getByPlaceId(placeId);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }

    static async delete(req: Request, res: Response, next: NextFunction) {
        try {
            const productId: string = req.params.id;
            const response = await service.delete(productId);
            res.status(Constants.HTTP_STATUS.OK).json(response);
        } catch (e) {
            next(e);
        }
    }
}
