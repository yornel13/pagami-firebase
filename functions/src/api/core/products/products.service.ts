import * as admin from 'firebase-admin';
import { Constants } from '../../../contants/contants';
import { Mappers } from '../../mappers/mappers';
import { ProductsRepository } from './products.repository';
import { ProductDto } from './domain/dto/product.dto';
import { PlacesService } from '../places/places.service';
import { ServerResponse } from '../base/server.response';

const repository = ProductsRepository;

export class ProductsService {

    static async save(productDto: ProductDto) {
        if (await PlacesService.validPlaceStatus(productDto.placeId, Constants.PLACE_STATUS.VERIFIED)) {
            productDto.createTime = admin.firestore.FieldValue.serverTimestamp();
            productDto.lastUpdate = admin.firestore.FieldValue.serverTimestamp();
            const id = await repository.save(productDto);
            const product = await repository.findById(id);
            return Mappers.responsePassed(product);
        } else {
            return Mappers.response(Constants.RESPONSE.CANT_UPDATED);
        }
    }

    static async changeStatus(productId: string, status: boolean) {
        const id = await repository.update(productId, {
            status,
            lastUpdate: admin.firestore.FieldValue.serverTimestamp()
        });
        const product = await repository.findById(id);
        return Mappers.responsePassed(product);
    }

    static async changeStock(productId: string, stock: number) {
        const id = await repository.update(productId, {
            stock,
            lastUpdate: admin.firestore.FieldValue.serverTimestamp()
        });
        const product = await repository.findById(id);
        return Mappers.responsePassed(product);
    }

    static async update(productDto: ProductDto) {
        if (await repository.exist(productDto.id!)) {
            productDto.lastUpdate = admin.firestore.FieldValue.serverTimestamp();
            const id = await repository.update(productDto.id!, productDto);
            const product = await repository.findById(id);
            return Mappers.responsePassed(product);
        } else {
            return Mappers.response(Constants.RESPONSE.CANT_UPDATED);
        }
    }

    /**
     * get products by place id
     */
    static async getByPlaceId(placeId: string): Promise<ServerResponse> {
        const objects = await repository.findByPlaceId(placeId);
        return Mappers.responsePassed(objects);
    }

    /**
     * delete product by id
     */
    static async delete(id: string): Promise<ServerResponse> {
       await repository.delete(id);
       return Mappers.responsePassed(Constants.RESPONSE.SUCCESS);
    }

    /**
     * Delete products by placeId
     */
    static async deleteByPlaceId(placeId: string): Promise<any> {
        await repository.deleteByPlaceId(placeId);
    }
}
