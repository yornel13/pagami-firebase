import { Constants } from '../../contants/contants';
import { Request, Response } from "express";
import { Mappers } from "../mappers/mappers";
import { ServerResponse } from "../core/base/server.response";

export class UserValidation {

    static isValid(req: Request, res: Response) {
        const response: ServerResponse = Mappers.response(
            Constants.RESPONSE.VALUE_INVALID,
            false,
            Constants.RESPONSE.CODE.VALUE_INVALID
        );
        const user = req.body;
        if (!user.name
            || !user.lastname
            || (!user.location || !this.isObject(user.location) || !user.location.country || user.location.country.length < 3)
            || !user.terms
            || !user.email) {
            res.status(Constants.HTTP_STATUS.OK).json(response);
            return false;
        }
        return true;
    }

    private static isObject(val: any) {
        return val instanceof Object;
    }
}
