import { Constants } from '../../contants/contants';
import { Request, Response } from "express";
import { Mappers } from "../mappers/mappers";
import { ServerResponse } from "../core/base/server.response";
import { PlaceResponse } from '../core/places/domain/response/place.response';

export class PlaceValidation {

    static isValid(req: Request, res: Response) {
        const response: ServerResponse = Mappers.response(
            Constants.RESPONSE.VALUE_INVALID,
            false,
            Constants.RESPONSE.CODE.VALUE_INVALID
        );
        const place: PlaceResponse = req.body;
        if (!place.name
            || !place.phone
            || !place.location) {
            res.status(Constants.HTTP_STATUS.OK).json(response);
            return false;
        }
        return true;
    }

    static isLocationValid(req: Request, res: Response) {
        const response: ServerResponse = Mappers.response(
            Constants.RESPONSE.VALUE_INVALID,
            false,
            Constants.RESPONSE.CODE.VALUE_INVALID
        );
        const place: PlaceResponse = req.body;
        if (!place.id
            || !place.latitude
            || !place.longitude
            || !place.location) {
            res.status(Constants.HTTP_STATUS.OK).json(response);
            return false;
        }
        return true;
    }

}
