import { Constants } from '../../contants/contants';
import { Request, Response } from "express";
import { Mappers } from "../mappers/mappers";
import { ServerResponse } from "../core/base/server.response";
import { ClaimDto } from '../core/claims/domain/dto/claim.dto';

export class ClaimValidation {

    static isValid(req: Request, res: Response) {
        const response: ServerResponse = Mappers.response(
            Constants.RESPONSE.VALUE_INVALID,
            false,
            Constants.RESPONSE.CODE.VALUE_INVALID
        );
        const claim: ClaimDto = req.body;
        if (!claim.placeId
            || !claim.businessEmail
            || !claim.businessPhone) {
            res.status(Constants.HTTP_STATUS.OK).json(response);
            return false;
        }
        return true;
    }

}
