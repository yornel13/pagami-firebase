import { Constants } from '../../contants/contants';
import { Request, Response } from "express";
import { Mappers } from "../mappers/mappers";
import { ServerResponse } from "../core/base/server.response";
import { ProductDto } from '../core/products/domain/dto/product.dto';

export class ServiceValidation {

    static isValid(req: Request, res: Response, update = false) {
        const response: ServerResponse = Mappers.response(
            Constants.RESPONSE.VALUE_INVALID,
            false,
            Constants.RESPONSE.CODE.VALUE_INVALID
        );
        const productDto: ProductDto = req.body;
        if (!productDto.placeId
            || !productDto.name
            || productDto.available === undefined
            || (!productDto.id && update)
            || !productDto.photoUrl) {
            res.status(Constants.HTTP_STATUS.OK).json(response);
            return false;
        }
        return true;
    }

}
